import { Pattern, Review } from '../src/types'

interface IBlueprintMockData {
   name: string
   metadata: {
      description: string
   }
   max: number | undefined
   interpretations: {
      tags: string[]
      interpretation: {
         src: string | undefined
         metadata: any
      }
   }[]
}

interface IspaceMockData {
   id: string
   title: string
   img: string
   genre: string
   shortDescription: string
   description: string
   gallery: string[]
   reviews: Review[]
   patterns?: Pattern[]
   spaceClient?: {
      data: string
      framework: string
      loader: string
      wasm: string
   }
}

export const MOCK_ADDRESS = '5FfA88n8kPDd9vH1D35H87kSsGECZ1sq5QiC5nYxD3VrEA89'

export const tags = [
   {
      tag: 'default-view',
      metadata: {
         id: 'default-view',
         description: 'The default representation of item',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'inventory-view',
      metadata: {
         id: 'inventory-view',
         description: 'The representation of item in inventory',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'animation',
      metadata: {
         id: 'animation',
         description: 'Representation of item with animation',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'fire-effect',
      metadata: {
         id: 'fire-effect',
         description: 'Adds fire-effect',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: '2d-sprite-atlas',
      metadata: {
         id: '2d-sprite-atlas',
         description: '2d picture representation',
         metadataExtensions: {
            fields: [
               {
                  name: 'tileHeight',
                  type: 'number',
                  editable: true,
                  defaultValue: 220,
                  description: 'The height of the sprite atlas tile',
               },
               {
                  name: 'tileWidth',
                  type: 'number',
                  editable: true,
                  defaultValue: 220,
                  description: 'The width of the sprite atlas tile',
               },
               {
                  name: 'columnCount',
                  type: 'number',
                  editable: true,
                  defaultValue: 8,
                  description: 'The number of sprites in the sprite atlas`s column',
               },
               {
                  name: 'rowCount',
                  type: 'number',
                  editable: true,
                  defaultValue: 9,
                  description: 'The number of sprites in the sprite atlas`s row',
               },
               {
                  name: 'pivotX',
                  type: 'number',
                  defaultValue: 0.5,
                  editable: true,
                  description: 'The pivot point X coordinate',
               },
               {
                  name: 'pivotY',
                  type: 'number',
                  defaultValue: 0,
                  editable: true,
                  description: 'The pivot point Y coordinate',
               },
            ],
         },
      },
   },
   {
      tag: '3d-model',
      metadata: {
         id: '3d-model',
         description: '3d model representation',
         metadataExtensions: {
            fields: [
               {
                  name: 'format',
                  type: 'string',
                  defaultValue: '.glb',
                  editable: false,
                  description: 'The format of source is .glb',
               },
            ],
         },
      },
   },
   {
      tag: 'blend',
      metadata: {
         id: 'blend',
         description: 'created by Blender 3D',
         metadataExtensions: {
            fields: [
               {
                  name: 'format',
                  type: 'string',
                  defaultValue: '.blend',
                  editable: false,
                  description: 'The format of source is .blend',
               },
            ],
         },
      },
   },
   {
      tag: 'png',
      metadata: {
         id: 'png',
         description: 'in PNG format',
         metadataExtensions: {
            fields: [
               {
                  name: 'format',
                  type: 'string',
                  defaultValue: '.png',
                  editable: false,
                  description: 'The format of source is PNG',
               },
            ],
         },
      },
   },
   {
      tag: 'jpeg',
      metadata: {
         id: 'jpeg',
         description: 'in JPEG format',
         metadataExtensions: {
            fields: [
               {
                  name: 'format',
                  type: 'string',
                  defaultValue: '.jpeg',
                  editable: false,
                  description: 'The format of source is JPEG',
               },
            ],
         },
      },
   },
   {
      tag: 'jpg',
      metadata: {
         id: 'jpg',
         description: 'in JPG format',
         metadataExtensions: {
            fields: [
               {
                  name: 'format',
                  type: 'string',
                  defaultValue: '.jpg',
                  editable: false,
                  description: 'The format of source is JPG',
               },
            ],
         },
      },
   },
   {
      tag: 'pixeled',
      metadata: {
         id: 'pixeled',
         description: 'pixeled style view',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'weapon-sword',
      metadata: {
         id: 'weapon-sword',
         description: 'weapon sword',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'skin',
      metadata: {
         id: 'skin',
         description: 'replace base in-space model to another',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'game-mechanic',
      metadata: {
         id: 'game-mechanic',
         description: 'describes in-space mechanic',
         metadataExtensions: {
            fields: [],
         },
      },
   },
   {
      tag: 'passive-earning',
      metadata: {
         id: 'passive-earning',
         description: 'Gives money passively when equipped',
         metadataExtensions: {
            fields: [
               {
                  name: 'tickDuration',
                  type: 'number',
                  defaultValue: '10',
                  editable: true,
                  description: 'Duration between two earns',
               },

               {
                  name: 'awardCount',
                  type: 'number',
                  defaultValue: '0',
                  editable: true,
                  description: 'Count of the getting awards',
               },
            ],
         },
      },
   },
   {
      tag: 'world-exploring',
      metadata: {
         id: 'world-exploring',
         description: 'helps explore in-space locations',
         metadataExtensions: {
            fields: [],
         },
      },
   },
]

export const blueprints: IBlueprintMockData[] = [
   {
      name: 'Old curved sword',
      metadata: {
         description:
            'Huge, curved sword. Each hit inflicts little damage, but fluid chain attacks are deadly."\n' +
            '"The sword\'s sharp slashing attacks are effective against cloth and flesh, but not against metal armor or tough scales.',
      },
      max: 100,
      interpretations: [
         {
            tags: ['default-view', 'png'],
            interpretation: {
               src: 'img/blueprints/sword/sword.png',
               metadata: {
                  description: 'The default representation of item | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['inventory-view', 'png'],
            interpretation: {
               src: 'img/blueprints/sword/sword-inventory-view.png',
               metadata: {
                  description: 'The inventory representation of item | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['2d-sprite-atlas', 'animation', 'png', 'weapon-sword', 'skin'],
            interpretation: {
               src: 'img/blueprints/sword/sword-2d-sprite-atlas.png',
               metadata: {
                  description: 'The sword sprite atlas',
                  tileHeight: 220,
                  tileWidth: 220,
                  columnCount: 8,
                  rowCount: 9,
                  pivotX: 0.5,
                  pivotY: 0,
                  format: '.png',
               },
            },
         },
         {
            tags: ['3d-model', 'weapon-sword'],
            interpretation: {
               src: 'img/blueprints/sword/sword.glb',
               metadata: {
                  description: '3d model representation',
                  format: '.glb',
               },
            },
         },
      ],
   },
   {
      name: 'Clover leaf',
      metadata: {
         description:
            'Clover or trefoil are common names for plants of the genus Trifolium, consisting of about 300 species of flowering plants in the legume or pea family Fabaceae originating in Europe. This NFT has magic ability to reward the player who put it on with gold.',
      },
      max: 100,
      interpretations: [
         {
            tags: ['default-view', 'png'],
            interpretation: {
               src: 'img/blueprints/clover-leaf/clover.png',
               metadata: {
                  description: 'The default representation of item | in PNG format',
                  tickDuration: 10,
                  awardAmount: 1,
                  format: '.png',
               },
            },
         },
         {
            tags: ['inventory-view', 'png', 'game-mechanic', 'passive-earning'],
            interpretation: {
               src: 'img/blueprints/clover-leaf/clover-inventory-view.png',
               metadata: {
                  description:
                     'The inventory representation of item | with passive earning space mechanic | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['3d-model', 'game-mechanic', 'passive-earning'],
            interpretation: {
               src: 'img/blueprints/clover-leaf/clover.glb',
               metadata: {
                  description: '3d model representation',
                  tickDuration: 10,
                  awardAmount: 1,
                  format: '.glb',
               },
            },
         },
      ],
   },
   {
      name: 'Kerosene lamp',
      metadata: {
         description:
            'A kerosene lamp (also known as a paraffin lamp in some countries) is a type of lighting device that uses kerosene as a fuel. Kerosene lamps have a wick or mantle as light source, protected by a glass chimney or globe.',
      },
      max: 100,
      interpretations: [
         {
            tags: ['default-view', 'png'],
            interpretation: {
               src: 'img/blueprints/kerosene-lamp/lamp.png',
               metadata: {
                  description: 'The default representation of item | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['inventory-view', 'png', 'game-mechanic', 'world-exploring'],
            interpretation: {
               src: 'img/blueprints/kerosene-lamp/lamp-inventory-view.png',
               metadata: {
                  description:
                     'The inventory representation of item | with world exploring space mechanic | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['3d-model', 'game-mechanic', 'world-exploring'],
            interpretation: {
               src: 'img/blueprints/kerosene-lamp/lamp.glb',
               metadata: {
                  description: '3d model representation',
                  format: '.glb',
               },
            },
         },
      ],
   },
   {
      name: 'Fire Sword',
      metadata: {
         description:
            'According to the Bible, a flaming sword was entrusted to the cherubim by God to guard the gates of Paradise after Adam and Eve were banished. Scholars have variously interpreted the sword as a weapon of the cherubim, as lightning, as a metaphor, as an independent divine being, or even as a figurative description of bladed chariot wheels.',
      },
      max: 100,
      interpretations: [
         {
            tags: ['default-view', 'png'],
            interpretation: {
               src: 'img/blueprints/fire-sword/fire-sword.png',
               metadata: {
                  description: 'The default representation of item | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['inventory-view', 'png'],
            interpretation: {
               src: 'img/blueprints/fire-sword/fire-sword-inventory-view.png',
               metadata: {
                  description: 'The inventory representation of item | in PNG format',
                  format: '.png',
               },
            },
         },
         {
            tags: ['2d-sprite-atlas', 'animation', 'png', 'weapon-sword', 'skin', 'fire-effect'],
            interpretation: {
               src: 'img/blueprints/fire-sword/fire-sword-2d-sprite-atlas.png',
               metadata: {
                  description: 'The sword sprite atlas',
                  tileHeight: 220,
                  tileWidth: 220,
                  columnCount: 8,
                  rowCount: 9,
                  pivotX: 0.5,
                  pivotY: 0,
                  format: '.png',
               },
            },
         },
         {
            tags: ['3d-model', 'weapon-sword', 'fire-effect'],
            interpretation: {
               src: 'img/blueprints/fire-sword/fire-sword-3d-model.glb',
               metadata: {
                  description: '3d model representation',
                  format: '.glb',
               },
            },
         },
      ],
   },
]

export const spaces: IspaceMockData[] = [
   {
      id: '0',
      title: 'Ninja Rian',
      img: 'img/spaces/ninja.png',
      genre: 'Platformer',
      shortDescription: 'Run jump slide and battle your way to a high score in Ninja Rian',
      description:
         'Play as a ninja who can run jump slide and throw ninja stars. Classic side scrolling endless runner. Battle against a huge and terrifying boss. Heaps of obstacles and traps to dodge! such as saw-blades, spinning axes, bombs and missiles!\nEnjoy endless hours of unlimited ninja running!\n - 4 different worlds\n - 40 different levels\n - 26 enemies in total\n',
      gallery: [
         'img/spaces/ninja-1.webp',
         'img/spaces/ninja-2.webp',
         'img/spaces/ninja-3.webp',
         'img/spaces/ninja-4.webp',
      ],
      reviews: [
         {
            id: '1',
            text: "The game is great and plays well...I'm giving it 4 stars because manual is not helpful at all...I've seen smaller spaces with much better manuals. But again, the game is great especially for the price.",
            rating: 4,
            address: MOCK_ADDRESS,
         },
         {
            id: '2',
            text: 'Great Game! I love to play Ninja Rian at school during breaks! World looks fantastic and game story is awesome!',
            rating: 5,
            address: MOCK_ADDRESS,
         },
         {
            id: '3',
            text: 'I am the best ninja Rian player. Please subscribe to my channel!!',
            rating: 5,
            address: MOCK_ADDRESS,
         },
      ],
      patterns: [
         {
            name: 'Weapon',
            description: 'Used in space for monster slaughtering and box destruction',
            img: 'img/patterns/sword-placeholder.png',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png'],
               },
               {
                  name: '2d sprite atlas',
                  tags: ['2d-sprite-atlas', 'animation', 'png', 'skin', 'weapon-sword'],
               },
            ],
         },
         {
            name: 'World explorer',
            description: 'Used in space for opening new locations',
            img: 'img/patterns/lamp-placeholder.png',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png', 'game-mechanic', 'world-exploring'],
               },
            ],
         },
         {
            name: 'Accessory',
            description: 'Used in game for passive money farming',
            img: 'img/patterns/clover-placeholder.png',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png', 'game-mechanic', 'passive-earning'],
               },
            ],
         },
      ],
      spaceClient: {
         data: 'data/ninja-rian/ReactBuild.data',
         framework: 'data/ninja-rian/ReactBuild.framework.js',
         loader: 'data/ninja-rian/ReactBuild.loader.js',
         wasm: 'data/ninja-rian/ReactBuild.wasm',
      },
   },
   {
      id: '1',
      title: 'Dark Forest',
      img: 'img/spaces/dark-forest/dark-forest-1.jpg',
      genre: 'RPG',
      shortDescription: 'Try to survive in a dangerous and inhospitable world of the Dark Forest!',
      description:
         'Tainted, and let grace guide you to wield the power of the Dark Forest and become a Lord in the Midlands. A vast and emotional world that connects open fields with various surprising situations and huge dungeons with complex and three-dimensional designs. Explore the unknown and face daunting threats to achieve a great sense of achievement.\n Traverse the breathtaking world on foot or on horseback, alone or online with other players, and fully immerse yourself in the grassy plains, suffocating swamps, spiraling mountains, foreboding castles and other sites of grandeur on a scale never seen before in a FromSoftware title.',
      gallery: [
         'img/spaces/dark-forest/dark-forest-1.jpg',
         'img/spaces/dark-forest/dark-forest-2.jpg',
         'img/spaces/dark-forest/dark-forest-3.jpg',
         'img/spaces/dark-forest/dark-forest-4.jpg',
      ],
      reviews: [
         {
            id: '1',
            text: "The game is great and plays well...I'm giving it 4 stars because manual is not helpful at all...I've seen smaller spaces with much better manuals. But again, the game is great especially for the price.",
            rating: 4,
            address: MOCK_ADDRESS,
         },
         {
            id: '2',
            text: 'Great Game! I love to play Ninja Rian at school during breaks! World looks fantastic and game story is awesome!',
            rating: 5,
            address: MOCK_ADDRESS,
         },
         {
            id: '3',
            text: 'I am the best ninja Rian player. Please subscribe to my channel!!',
            rating: 5,
            address: MOCK_ADDRESS,
         },
      ],
      patterns: [
         {
            name: 'Sword',
            description: 'Used in game for monster slaughtering and box destruction',
            img: 'img/patterns/dark-forest/sword-placeholder.jpg',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png'],
               },
               {
                  name: '3d model',
                  tags: ['3d-model', 'weapon-sword'],
               },
            ],
         },
         {
            name: 'World explorer',
            description: 'Used in space for opening new locations',
            img: 'img/patterns/dark-forest/lamp-placeholder.jpg',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png', 'game-mechanic', 'world-exploring'],
               },
               {
                  tags: ['3d-model', 'game-mechanic', 'world-exploring'],
               },
            ],
         },
         {
            name: 'Accessory',
            description: 'Used in game for passive money farming',
            img: 'img/patterns/dark-forest/clover-placeholder.jpg',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
               {
                  tags: ['inventory-view', 'png', 'game-mechanic', 'passive-earning'],
               },
               {
                  tags: ['3d-model', 'game-mechanic', 'passive-earning'],
               },
            ],
         },
      ],
      spaceClient: {
         data: 'data/dark-forest/ReactBuild.data',
         framework: 'data/dark-forest/ReactBuild.framework.js',
         loader: 'data/dark-forest/ReactBuild.loader.js',
         wasm: 'data/dark-forest/ReactBuild.wasm',
      },
   },
   {
      id: '2',
      title: 'Fortnite',
      img: 'img/spaces/fortnite.jpg',
      genre: 'Free-to-play Battle Royale',
      shortDescription: 'Join the Resistance in the final battle to free the Zero Point!',
      description:
         'The IO has lined up guards and sky stations, but the Resistance is equipped with new tactics like sprinting, mantling, and more. Board an Armored Battle Bus to be a powerful force or attach a Cow Catcher to your truck for extra ramming power. Take on your opponents in the ultimate battle for the Zero Point in Chapter 3 Season 2: Resistance!\n' +
         'The Chapter 3 Season 2 Battle Pass includes the Master of the Mystic Arts, Doctor Strange. Joining him in the Battle Pass are characters like Tsuki 2.0, the familiar foe Gunnar, and revealed at last: The Origin. Also, complete special Quests to “reprogram” the Omni Sword Pickaxe. Configure it with a different blade, guard, color, and even sound!',
      gallery: [
         'img/spaces/fortnite-1.jpg',
         'img/spaces/fortnite-2.webp',
         'img/spaces/fortnite-3.png',
         'img/spaces/fortnite-4.jpg',
         'img/spaces/fortnite-5.webp',
      ],
      reviews: [
         {
            id: '1',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            rating: 3.5,
            address: MOCK_ADDRESS,
         },
         {
            id: '2',
            text: 'Proin condimentum dapibus libero quis molestie. Fusce a turpis ut turpis hendrerit pellentesque. Quisque in odio eu nulla rutrum laoreet. Donec vitae vehicula eros, sed luctus metus. Fusce quis neque dictum, ornare dui sed, vulputate purus. Donec porta tortor condimentum velit volutpat consectetur. Integer porttitor nulla in nisl laoreet, sit amet porta quam ultrices. Duis at tempor urna. Vestibulum nec convallis neque. Vivamus auctor aliquam aliquam. Maecenas eu arcu urna.',
            rating: 4.5,
            address: MOCK_ADDRESS,
         },
      ],
      patterns: [
         {
            name: 'skin',
            description: 'Change appearance for items in inventory',
            img: 'img/blueprints/skin/skin.jpg',
            interpretations: [
               {
                  tags: ['default-view', 'jpg', 'skin'],
               },
            ],
         },
         {
            name: 'Avatar',
            description: "Shows player's avatar for other players",
            img: 'img/blueprints/skin/skin.jpg',
            interpretations: [
               {
                  tags: ['default-view', 'png'],
               },
            ],
         },
         {
            name: 'Extended accessory',
            description: 'Used in game for passive money farming',
            img: 'img/patterns/clover-placeholder.png',
            interpretations: [
               {
                  tags: ['default-view', 'png', 'game-mechanic', 'passive-earning'],
               },
               {
                  tags: ['3d-model', 'game-mechanic', 'passive-earning'],
               },
            ],
         },
      ],
   },
   {
      id: '3',
      title: 'GTA V',
      img: 'img/spaces/gta-v.jpg',
      genre: 'Entertainment blockbusters',
      shortDescription:
         'When a young street hustler, a retired bank robber, and a terrifying psychopath find themselves entangled with some of the most frightening and deranged elements of the criminal underworld, the U.S. government, and the entertainment industry, they must pull off a series of dangerous heists to survive in a ruthless city in whih they can trust nobody — least of all each other.',
      description:
         "Players use melee attacks, firearms and explosives to fight enemies, and may run, jump, swim or use vehicles to navigate the world. To accommodate the map's size, the game introduces vehicle types absent in its predecessor Grand Theft Auto IV, such as fixed-wing aircraft. In combat, auto-aim and a cover system may be used as assistance against enemies.\n" +
         'Should players take damage, their health meter will gradually regenerate to its halfway point. Players respawn at hospitals when their health depletes. If players commit crimes, law enforcement agencies may respond as indicated by a "wanted" meter in the head-up display (HUD).',
      gallery: [
         'img/spaces/gta-v-1.jpg',
         'img/spaces/gta-v-2.jpg',
         'img/spaces/gta-v-3.jpeg',
         'img/spaces/gta-v-4.jpg',
         'img/spaces/gta-v-5.png',
      ],
      reviews: [],
   },
   {
      id: '4',
      title: 'Last of Us II',
      img: 'img/spaces/last-of-us-2.png',
      genre: 'Survival game',
      shortDescription:
         "Confront the devastating physical and emotional repercussions of Ellie's actions.",
      description:
         'Five years after their dangerous journey across the post-pandemic United States, Ellie and Joel have settled down in Jackson, Wyoming. Living amongst a thriving community of survivors has allowed them peace and stability, despite the constant threat of the infected and other, more desperate survivors.\n' +
         'When a violent event disrupts that peace, Ellie embarks on a relentless journey to carry out justice and find closure. As she hunts those responsible one by one, she is confronted with the devastating physical and emotional repercussions of her actions.',
      gallery: [
         'img/spaces/last-of-us-2-1.jpg',
         'img/spaces/last-of-us-2-2.jpg',
         'img/spaces/last-of-us-2-3.jpg',
         'img/spaces/last-of-us-2-4.jpg',
         'img/spaces/last-of-us-2-5.jpg',
      ],
      reviews: [],
   },
   {
      id: '5',
      title: 'Minecraft',
      img: 'img/spaces/minecraft.png',
      genre: '3D sandbox game',
      shortDescription: 'Minecraft is a sandbox video game developed by Mojang Studios.',
      description:
         'In Minecraft, players explore a blocky, procedurally generated 3D world with virtually infinite terrain, and may discover and extract raw materials, craft tools and items, and build structures, earthworks and simple machines. \n' +
         'Depending on game mode, players can fight computer-controlled mobs, as well as cooperate with or compete against other players in the same world. Game modes include a survival mode, in which players must acquire resources to build the world and maintain health, and a creative mode where players have unlimited resources and access to flight.',
      gallery: [
         'img/spaces/minecraft-1.jpeg',
         'img/spaces/minecraft-2.webp',
         'img/spaces/minecraft-3.jpeg',
         'img/spaces/minecraft-4.jpg',
         'img/spaces/minecraft-5.jpg',
         'img/spaces/minecraft-6.jpg',
      ],
      reviews: [],
   },
   {
      id: '6',
      title: 'Call of Duty: WWII',
      img: 'img/spaces/call-of-duty.webp',
      genre: 'First-person shooter',
      shortDescription:
         'A breathtaking experience that redefines World War II for a new gaming generation',
      description:
         'The game simulates the infantry and combined arms warfare of World War II. An expansion pack, Call of Duty: United Offensive, was developed by Gray Matter Interactive with contributions from Pi Studios and produced by Activision. The game follows American and British paratroopers and the Red Army. \n' +
         'The Mac OS X version of the game was ported by Aspyr Media. In late 2004, the N-Gage version was developed by Nokia and published by Activision. ',
      gallery: [
         'img/spaces/call-of-duty-1.jpg',
         'img/spaces/call-of-duty-2.jpg',
         'img/spaces/call-of-duty-3.jpg',
         'img/spaces/call-of-duty-4.jpg',
         'img/spaces/call-of-duty-5.webp',
      ],
      reviews: [],
   },
   {
      id: '7',
      title: 'RDR2',
      img: 'img/spaces/rdr-2.jpeg',
      genre: 'Western action-adventure',
      shortDescription:
         'America, 1899. The end of the wild west era has begun as lawmen hunt down the last remaining outlaw gangs. Those who will not surrender or succumb are killed.',
      description:
         'After a robbery goes badly wrong in the western town of Blackwater, Arthur Morgan and the Van der Linde gang are forced to flee. With federal agents and the best bounty hunters in the nation massing on their heels, the gang must rob, steal and fight their way across the rugged heartland of America in order to survive. As deepening internal divisions threaten to tear the gang apart, Arthur must make a choice between his own ideals and loyalty to the gang who raised him.\n' +
         'From the creators of Grand Theft Auto V and Red Dead Redemption, Red Dead Redemption 2 is an epic tale of life in America at the dawn of the modern age. Out now on Playstation 4, Xbox One, PC, and Stadia.',
      gallery: [
         'img/spaces/rdr-2-1.avif',
         'img/spaces/rdr-2-2.jpg',
         'img/spaces/rdr-2-3.webp',
         'img/spaces/rdr-2-4.webp',
         'img/spaces/rdr-2-5.jpg',
      ],
      reviews: [],
   },
   {
      id: '8',
      title: 'Stalker',
      img: 'img/spaces/stalker.jpg',
      genre: 'First-person shooter',
      shortDescription:
         'Next-gen sequel to the award-winning PC game franchise developed by GSC Game World, set to deliver a unique action experience of survival in the post-apocalyptic Chornobyl Exclusion Zone.',
      description:
         'The Heart of Chornobyl has never been beating so loud as it is right now. The Zone is calling for stalkers, who are ready to delve into the non-linear journey through a sinister open world in the Eastern European post-apocalyptic setting.\n' +
         "S.T.A.L.K.E.R. 2: Heart of Chornobyl applies the full potential of Unreal Engine 5 as well as motion capture and photogrammetry technologies to provide you with benchmark-setting graphics and ultimate immersion into the game world. The advanced artificial intelligence system will compel the stalker to utilize a tactical approach against numerous enemies, challenging and keeping even the most hard-boiled players engaged. A-Life 2.0 life-simulating system builds a holistic live environment where player's actions have an impact on the world of the Zone.",
      gallery: [
         'img/spaces/stalker-1.jpg',
         'img/spaces/stalker-2.jpg',
         'img/spaces/stalker-3.jpg',
         'img/spaces/stalker-4.jpeg',
         'img/spaces/stalker-5.jpg',
      ],
      reviews: [],
   },
   {
      id: '9',
      title: 'Metro 2033',
      img: 'img/spaces/metro-2033.webp',
      genre: 'First-person life simulation',
      shortDescription:
         "The story is based on Dmitry Glukhovsky's novel of the same name, where survivors of a nuclear war have taken refuge in the Metro tunnels of Moscow.",
      description:
         'The human and mutant enemies can be killed with a variety of firearms. The game features traditional guns like a revolver, assault rifles and shotguns, as well as more inventive weapons like a pneumatic crossbow. In firefights, human enemies take cover and flank the player, while mutant enemies stay in the open and try to bite them. Alternatively, the player can employ stealth to evade their enemies or kill them silently. This can be achieved by using a throwing knife to kill an enemy from afar, or shooting an enemy with a suppressed weapon.\n' +
         'Since the game has a large survival horror focus, the player often has little ammunition, and must scavenge for supplies from caches or dead bodies. An essential supply is pre-war 5.45×39mm ammunition (referred to in the game as "military grade ammunition"), which is also the main currency in the tunnels. This ammunition can be traded for weapons and upgrades, or used directly as stronger bullets than other scavenged ammunition.',
      gallery: [
         'img/spaces/metro-2033-1.webp',
         'img/spaces/metro-2033-2.webp',
         'img/spaces/metro-2033-3.jpg',
         'img/spaces/metro-2033-4.jpg',
         'img/spaces/metro-2033-5.jpg',
         'img/spaces/metro-2033-6.jpg',
      ],
      reviews: [],
   },
]
