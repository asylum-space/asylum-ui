import { AsylumApi, IAsylumApi, InterpretationCreate } from '../src'
import { getFile } from '../src/utils'
import {
   blueprints as blueprintsMockData,
   spaces as spacesMockData,
   tags as tagsMockData,
} from './mocks'
import { Keyring } from '@polkadot/api'
import { KeyringPair } from '@polkadot/keyring/types'
import * as dotenv from 'dotenv'
import { readFileSync } from 'fs'

dotenv.config()

function toEntries<T>(a: T[]) {
   return a.map((value, index) => [index, value] as const)
}

const prepareSeeder = async (api: IAsylumApi): Promise<KeyringPair> => {
   const seeder = new Keyring({ type: 'sr25519' }).addFromUri(process.env.SEEDER_MNEMONIC || '')
   const alice = new Keyring({ type: 'sr25519' }).addFromUri('//Alice')

   await api
      .withKeyringPair(alice)
      .signAndSendWrapped(api.polkadotApi.tx.balances.transfer(seeder.address, 10 ** 12))

   return seeder
}

const seed = async (api: IAsylumApi, seeder: KeyringPair): Promise<void> => {
   console.log('Starting seed...')

   await seedTags(api)
   await seedBlueprints(api)
   await seedSpaces(api)
   await seedItems(api, seeder)

   console.log('Seed finished')
}

const seedTags = async (api: IAsylumApi): Promise<void> => {
   try {
      console.log('Initializing tags...')

      for (const [index] of toEntries(tagsMockData)) {
         const entry = tagsMockData[index]

         const metadataCID = await api.uploadMetadata(entry.metadata)
         await api.createInterpretationTag(entry.tag, metadataCID)

         console.log(`Added tag '${entry.tag}' with metadata:`)
      }

      console.log('[Initializing tags] SUCCEED')
   } catch (error) {
      console.error('[Initializing tags] FAILED')
      console.error('[Initializing tags] Error: ' + error)
   }
}

const seedBlueprints = async (api: IAsylumApi): Promise<void> => {
   try {
      console.log('Initializing blueprints...')

      for (const blueprint of blueprintsMockData) {
         const blueprintMetadataCID = await api.uploadMetadata(blueprint.metadata)

         const interpretations: InterpretationCreate[] = await Promise.all(
            blueprint.interpretations.map(async (interpretation) => {
               const metadataCID = await api.uploadMetadata(interpretation.interpretation.metadata)

               const image = readFileSync(interpretation.interpretation.src!)
               const srcCID = await api.uploadFile(image)

               return {
                  tags: interpretation.tags,
                  interpretation: {
                     src: srcCID,
                     metadata: metadataCID,
                  },
               }
            })
         )

         await api.createBlueprint(
            blueprint.name,
            blueprintMetadataCID,
            blueprint.max,
            interpretations
         )

         console.log(
            `Added blueprint '${blueprint.name}' with metadata: '${blueprintMetadataCID}'}`
         )
      }
      console.log('[Initializing blueprints] SUCCEED')
   } catch (error) {
      console.error('[Initializing blueprints] FAILED')
      console.error('[Initializing blueprints] Error: ' + error)
   }
}

const seedItems = async (api: IAsylumApi, seeder: KeyringPair): Promise<void> => {
   try {
      console.log('Initializing items...')

      const blueprints = await api.blueprints()

      for (const blueprint of blueprints) {
         const metadata = await getFile(
            process.env.IPFS_NODE_URL?.replace(':5001', ':8080') || '',
            blueprint.metadata
         )
         const metadataCIDArr = await Promise.all(
            [0, 1, 2].map((index) => {
               return api.uploadMetadata({
                  ...metadata,
                  name: `${blueprint.name}: NFT Item ${index}`,
               })
            })
         )
         for (const metadataCID of metadataCIDArr) {
            await api.mintItem(seeder.address, blueprint.id, metadataCID)
         }
         console.log(
            `All items from blueprint ${blueprint.name}: `,
            await api.itemsByBlueprint(blueprint.id)
         )
      }

      console.log('[Initializing items] SUCCEED')
   } catch (error) {
      console.error('[Initializing items] FAILED')
      console.error('[Initializing items] Error: ' + error)
   }
}

const seedSpaces = async (api: IAsylumApi): Promise<void> => {
   try {
      console.log('Initializing spaces...')

      for (const [index] of toEntries(spacesMockData)) {
         const entry = spacesMockData[index]

         const image = readFileSync(entry.img)
         entry.img = await api.uploadFile(image)

         for (let i = 0; i < entry.gallery.length; i++) {
            const image = readFileSync(entry.gallery[i])
            entry.gallery[i] = await api.uploadFile(image)
         }

         if (entry.patterns) {
            for (const pattern of entry.patterns) {
               pattern.img = await api.uploadFile(readFileSync(pattern.img))
            }
         }

         if (entry.spaceClient) {
            console.log('Initializing space client: ' + entry.title)

            const data = readFileSync(entry.spaceClient.data)
            const loader = readFileSync(entry.spaceClient.loader)
            const framework = readFileSync(entry.spaceClient.framework)
            const wasm = readFileSync(entry.spaceClient.wasm)
            entry.spaceClient.data = await api.uploadFile(data)
            entry.spaceClient.loader = await api.uploadFile(loader)
            entry.spaceClient.framework = await api.uploadFile(framework)
            entry.spaceClient.wasm = await api.uploadFile(wasm)
         }

         await api.createSpace(index, [api.keyringPair?.address || ''], 0)
         const spaceCID = await api.uploadMetadata(entry)
         await api.setSpaceMetadata(index, spaceCID, entry.title, entry.genre)

         console.log(`Added space '${entry.title}' with metadata:`)
         console.log(await api.spaceMetadataOf(index))

         console.log(`Space '${entry.title}':`)
         console.log(await api.space(index))
      }

      console.log('[Initializing spaces] SUCCEED')
   } catch (error) {
      console.error('[Initializing spaces] FAILED')
      console.error('[Initializing spaces] Error: ' + error)
   }
}

AsylumApi.connect(
   { nodeUrl: process.env.ASYLUM_NODE_URL || '', ipfsUrl: process.env.IPFS_NODE_URL || '' },
   () => {},
   () => {},
   {
      throwOnConnect: false,
      throwOnUnknown: false,
   }
)
   .then(async (api) => {
      await api.api?.isReady
      const seeder = await prepareSeeder(api)
      await seed(api.withKeyringPair(await prepareSeeder(api)), seeder)
      terminate(0)
   })
   .catch((err) => {
      console.error(err)
      terminate(1)
   })

const terminate = (exitCode: number): void => {
   process.exit(exitCode)
}

export {}
