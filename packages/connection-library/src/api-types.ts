export const apiTypes = {
   InterpretationId: 'u32',
   BoundedString: 'BoundedVec<u8, u32>',
   BoundedTag: 'BoundedVec<u8, u32>',
   BoundedTags: 'BoundedBTreeSet<BoundedTag, u32>',
   IntepretationInfo: {
      src: 'Option<BoundedString>',
      metadata: 'Option<BoundedString>',
      license: 'Option<BoundedString>',
      thumb: 'Option<BoundedString>',
   },
   Intepretation: {
      tags: 'BoundedTags',
      interpretation: 'IntepretationInfo',
   },
   Add: {
      interpretations: 'BoundedVec<Intepretation, u32>',
   },
   Modify: {
      interpretations: 'BoundedVec<(InterpretationId, Intepretation), u32>',
   },
   RemoveInterpretation: {
      interpretationId: 'InterpretationId',
   },
   Change: {
      _enum: {
         Add: 'Add',
         Modify: 'Modify',
         RemoveInterpretation: 'RemoveInterpretation',
      },
   },
}
