# Connection Library Testing Guide

This testing guide will go through the same flow as the [Asylum node Testing Guide](https://gitlab.com/asylum-space/asylum-item-nft/-/blob/main/docs/testing-guide.md) but using the Connection Library.

Almost all functionality of the Connection Library is covered in the [seed script](https://gitlab.com/asylum-space/asylum-ui/-/blob/main/packages/connection-library/seed/index.ts) and can be used as a reference.

> Note: interactions with NFT items are not supported in the current version of the Connection Library

### Installation

-  Install and run [Asylum node](https://gitlab.com/asylum-space/asylum-item-nft/-/blob/main/README.md)
-  Add [Connection Library](https://gitlab.com/asylum-space/asylum-ui/-/tree/main/packages/connection-library) as a local dependency package in your npm project.

### Prepare account

To start working with the Connection Library we need to prepare an account:

```js
import { AsylumApi } from '@asylum-ui/connection-library'

const nodeUrl = "ws://127.0.0.1:9944"
const ipfsUrl = "http://127.0.0.1:5001"

const api = AsylumApi.connect({ nodeUrl, ipfsUrl })
   .then(async (api) => {
        const mnemonic = "eternal danger cherry radar exit damage slam hip say relief awesome middle"

        const testAccount = new Keyring({ type: 'sr25519' }).addFromUri(mnemonic)
        const alice = new Keyring({ type: 'sr25519' }).addFromUri('//Alice')

        await api
           .withKeyringPair(alice)
           .signAndSendWrapped(api.polkadotApi.tx.balances.transfer(seeder.address, 10 ** 12))

        return api.withKeyringPair(testAccount)
   }
```

All subsequent steps will be performed with the current preset.

### Tags

To create new tags, you need to upload tag's metadata to IPFS and call `createInterpretationTag`. In the example, we're creating a couple of tags `default-view`,`jpeg`:

```js
// 1. Upload to IPFS `default-view` tag metadata and get its CID:
const defaultViewTagMetadata = {
  "id": "default-view",
  "description": "The default visualization for the item. MUST be present in all NFTs.",
  "metadataExtensions": {}
}
const defaultViewTagMetadataCID = await api.uploadMetadata(defaultViewTagMetadata)

// 2. Create a `default-view` tag:
await api.createInterpretationTag('default-view', defaultViewTagMetadataCID)

// 3. Upload to IPFS `jpeg` tag metadata and get its CID:
const jpegTagMetadata = {
  "id": "jpeg",
  "description": "in .jpeg format",
  "metadataExtensions": {
      "fileds": [
        {
          "name": "format",
          "type": "string",
          "default": ".jpeg",
          "description": "The format of source is JPEG"
        }
      ]
  }
}
const jpegTagMetadataCID = await api.uploadMetadata(jpegTagMetadata)

// 4. Create a `jpeg` tag:
await api.createInterpretationTag('jpeg', jpegTagMetadataCID)

### Blueprint

Now we can create a blueprint with interpretations that support tags created in the previous step. To do this, we need to call `createBlueprint`.

```js
// 1. Upload blueprint metadata to IPFS and get its CID:
const blueprintMetadata = {
  "description": "The best weapon for the Helloween party 2022",
}
const blueprintMetadataCID = await api.uploadMetadata(blueprintMetadata)

// 2. Upload interpretation metadata to IPFS and get its CID:
const interpretationMetadata = {
  "description": "Default view interpretation in JPG format",
  "format": ".jpg"
}
const interpretationMetadataCID = await api.uploadMetadata(interpretationMetadata)

// 3. Upload interpretation source to IPFS and get its CID.
const srcCID = "{INTERPRETATION_SOURCE_CID}"

// 4. Call `createBlueprint` method:
const interpretations = [
     {
        "tags": ["default-view", "jpeg"],
        "interpretation": {
           "id": "default-view-jpg",
           "src": srcCID,
           "metadata": interpretationMetadataCID,
        },
     },
]
const maxItems = 100
await api.createBlueprint("Old sword", blueprintMetadataCID, maxItems, interpretations)

// 5. Retrieve data about all interpretations of the created blueprint:
const blueprintId = '0'
console.log(await api.blueprintInterpretations(blueprintId))
```

### space

The following listing shows how to create and configure a space:

```js
// 1. Create a space:
const spaceId = 0,
const admins = [api.keyringPair!.address],
const price = 10000
await api.createspace(spaceId, admins, price)

// 2. Set space metadata:
const spaceMetadata = {
    title: 'Minecraft',
    img: '{link-to-cover}',
    genre: '3D sandbox space',
    shortDescription: 'Minecraft is a sandbox video space developed by Mojang Studios.',
    description: 'Really long description',
    gallery: [
         '{link-to-screenshot}',
         '{link-to-screenshot}',
         '{link-to-art}',
      ],
      reviews,
}
const spaceMetadataCID = await api.uploadMetadata(spaceMetadata)
await api.setspaceMetadata(spaceId, spaceMetadataCID, spaceMetadata.title, spaceMetadata.genre)

// 3. We suppose that our space supports the "Old sword" blueprint.
// Call `addBlueprintSupport` to add association between the space and blueprint:
await api.addBlueprintSupport(spaceId, blueprintId)

// 4. Retrieve space data and metadata:
console.log(await api.space(spaceId))
console.log(await api.spaceMetadataOf(spaceId))
```

### Update blueprint

When the blueprint already exists and items are minted, we still have a possibility to edit it - extend with new interpretations or fix the old ones.

Let's assume that we want to add a 3d model representation for the "Old sword" blueprint to make it usable in 3d spaces and also update the link for 2d interpretation.

> Note: to continue the guide, you need to create all necessary tags for the 3d model (`3d-model`, `obj`) as described in the Tags section **before** moving forward.

1. **Submit proposal**

To do this, anybody can submit a blueprint change proposal. Call `submitBlueprintChangeProposal` with two changes - `Add` and `Modify`:

```js
const blueprintId = 0,
const author = "{AUTHOR_ACCOUNT_ID}",
const changeSet = [
   new BlueprintChangeAdd([
      {
         tags: ['3d-model', 'obj'],
         interpretation: {
            id: "3d-model-obj",
            src: "{3D_INTERPRETATION_SOURCE_CID}",
            metadata: "{3D_INTERPRETATION_METADATA_CID}",
         },
      },
   ]),
   new BlueprintChangeModify([
      [
         'default-view-jpg54',
         {
            tags: ['default-view', 'jpg'],
            interpretation: {
               id: 'default-view-jpg55',
               src: '{NEW_INTERPRETATION_SOURCE_CID}',
               metadata: '{METADATA_CID}',
            }
         },
      ]
   ]),
],

await api.submitBlueprintChangeProposal(author, blueprintId, changeSet)
```

-  In the `Modify` change action we're describing the changes of source or metadata of already existing interpretation.
-  With the `Add` change, we're adding a new interpretation to the blueprint.

There are also `RemoveInterpretation` option, that can be used in a similar way.

2. **Wait for the proposal approved**

Let's assume DAO accepted that proposal (currently done automatically after submitting the proposal)

3. **Update blueprint**

Now the blueprint's owner can call `update_blueprint` extrinsic with the `blueprintId` and `proposalId`, and all proposed updates will be applied to the blueprint.

```js
const proposalId = 0

await api.updateBlueprint(blueprintId, proposalId)
```
