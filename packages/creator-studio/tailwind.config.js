const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
   content: ['./src/**/**/*.{ts,tsx}'],
   corePlugins: {
      container: false,
   },
   theme: {
      fontSize: {
         sm: ['0.75rem', '1.125rem'], // 12px
         base: ['0.875rem', '1.25rem'], // 14px
         lg: ['1rem', '1.5rem'], // 16px
         xl: ['1.375rem', '1.375rem'], // 24px
         '2xl': ['1.75rem', '3rem'], // 28px
      },
      extend: {
         backgroundImage: {
            'gradient-hr':
               'linear-gradient(90deg, rgba(48, 48, 48, 0) 0%, rgba(255, 255, 255, 1) 20%, rgba(250, 250, 250, 1) 80%, rgba(48, 48, 48, 0) 100%)',
            'gradient-backdrop':
               'linear-gradient(hsla(194, 32%, 72%, 0.25) 28.65%, hsla(221, 14%, 47%, 0.25) 100%)',
            'gradient-grayish': 'linear-gradient(270deg, #4D4D4D 37.5%, #303030 70.31%)',
            'gradient-to-transparency':
               'linear-gradient(hsla(0, 0%, 11%, 0), hsla(0, 0%, 11%, 1) 60% 100%)',
            'radial-gradient-grayish':
               'radial-gradient(50% 50% at 50% 50%, #131313 0%, #303030 75.52%)',
            'gradient-app': 'linear-gradient(to bottom, hsla(0, 0%, 8%, 1), hsla(0, 0%, 3%, 1))',
         },
         fontFamily: {
            sans: ['ubuntu', ...defaultTheme.fontFamily.sans],
            secondary: ['Oxanium', ...defaultTheme.fontFamily.sans],
         },
         colors: {
            asylum: {
               magenta: '#8D7AEC',
               'sky-blue': '#50BFFF',
               blue: '#4396C6',
            },
            neutral: {
               100: '#F2F2F2',
               300: '#DFDFDF',
               800: '#292929',
               900: '#1C1C1C',
               950: '#101010',
            },
            yellow: {
               400: '#EAC25C',
            },
         },
         borderRadius: {
            DEFAULT: '0.3125rem',
            md: '0.46875rem',
            lg: '0.625rem',
            xl: '0.9375rem',
         },
         boxShadow: {
            glitch:
               '-0.25rem 0.25rem  hsla(288, 100%, 50%, 0.75), 0.25rem -0.25rem  hsla(202, 100%, 66%, 0.75)',
         },
         lineClamp: {
            9: '9',
         },
         keyframes: {
            'slide-out': {
               '0%': {
                  transform: 'translateY(0)',
                  background: 'rgba(48, 48, 48, 0.7)',
               },
               '30%': {
                  transform: 'translateY(0)',
                  background: 'transparent',
               },
               '100%': {
                  transform: 'translateY(100%)',
                  background: 'transparent',
               },
            },
            'slide-in': {
               '0%': {
                  transform: 'translateY(100%)',
                  background: 'transparent',
               },
               '70%': {
                  transform: 'translateY(0)',
                  background: 'transparent',
               },
               '100%': {
                  transform: 'translateY(0)',
                  background: 'rgba(48, 48, 48, 0.7)',
               },
            },
            'fade-in': {
               '0%': {
                  opacity: '0',
               },
               '100%': {
                  opacity: '1',
               },
            },
            'fade-out': {
               '0%': {
                  opacity: '1',
               },
               '100%': {
                  opacity: '0',
               },
            },
         },
         scale: {
            mirror: '-1',
         },
         animation: {
            'slide-out': 'slide-out 0.7s ease-in forwards',
            'slide-in': 'slide-in 0.7s ease-in forwards',
            'fade-in': 'fade-in 0.15s cubic-bezier(0.4, 0, 0.2, 1) forwards',
            'fade-out': 'fade-out 0.15s cubic-bezier(0.4, 0, 0.2, 1) forwards',
         },
      },
   },
   plugins: [
      require('@tailwindcss/line-clamp'),
      function ({ addComponents }) {
         addComponents({
            '.container': {
               maxWidth: '100%',
               '@screen sm': {
                  maxWidth: '100%',
               },
               '@screen md': {
                  maxWidth: '700px',
               },
               '@screen lg': {
                  maxWidth: '800px',
               },
               '@screen xl': {
                  maxWidth: '940px',
               },
               '@screen 2xl': {
                  maxWidth: '1090px',
               },
            },
         })
      },
   ],
}
