describe('Blueprints', () => {
   before(() => {
      cy.resetDocker()
   })

   it('renders item overview', () => {
      cy.authorize()
      cy.findByRole('button', { name: /items/i }).click()
      cy.contains(/Old curved sword: NFT Item 1/i).click()

      cy.findByRole('heading', { level: 2 }).should('have.text', 'Old curved sword: NFT Item 1')
      cy.findByText(/owner/i).parent().findByText('5ELY...HMCN').should('be.visible')
      cy.findAllByTestId('tag-group').should((items) => {
         expect(items).to.have.length(4)
         expect(items.eq(0)).to.include.text(['default-view', 'png'].join(''))
         expect(items.eq(1)).to.include.text(['3d-model', 'weapon-sword'].join(''))
         expect(items.eq(2)).to.include.text(['inventory-view', 'png'].join(''))
         expect(items.eq(3)).to.include.text(
            ['2d-sprite-atlas', 'animation', 'png', 'skin', 'weapon-sword'].join('')
         )
      })
   })

   it('mints item', () => {
      cy.authorize()
      cy.findByRole('button', { name: /blueprints/i }).click()

      cy.contains(/old curved sword/i)
         .findByRole('button', { name: /mint/ })
         .click()
      cy.findByLabelText(/name/i, { selector: 'input' }).clear().type('Sword NFT')
      cy.findByTestId('modal')
         .findByRole('button', { name: /mint item/i })
         .click()

      cy.findByRole('button', { name: /items/i }).click()
      cy.contains(/Old curved sword: NFT Item 1/i).click()
      cy.findAllByTestId('tag-group').should((items) => {
         expect(items).to.have.length(4)
         expect(items.eq(0)).to.include.text(['default-view', 'png'].join(''))
         expect(items.eq(1)).to.include.text(['3d-model', 'weapon-sword'].join(''))
         expect(items.eq(2)).to.include.text(['inventory-view', 'png'].join(''))
         expect(items.eq(3)).to.include.text(
            ['2d-sprite-atlas', 'animation', 'png', 'skin', 'weapon-sword'].join('')
         )
      })
   })
})

export {}
