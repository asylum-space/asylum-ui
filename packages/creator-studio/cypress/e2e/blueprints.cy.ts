describe('Items', () => {
   before(() => {
      cy.resetDocker()
   })

   it('creates blueprint', () => {
      const blueprintName = 'Fire sword test'
      cy.authorize()
      cy.findByRole('button', { name: /blueprints/i }).click()
      cy.findByRole('button', { name: /create blueprint/i }).click()

      cy.findByLabelText(/name/i, { selector: 'input' }).type(blueprintName)
      cy.findByLabelText(/description/i, { selector: 'textarea' }).type('Super flaming sword')
      cy.findByTestId('interpretation-select').click()
      cy.findByTestId('interpretation-select').findByText('png').click()
      cy.findByTestId('interpretation-file-upload').selectFile('cypress/fixtures/fire-sword.png', {
         action: 'drag-drop',
      })
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(1000)
      cy.findByTestId('modal')
         .findByRole('button', { name: /create blueprint/i })
         .click()

      cy.findByText(`Blueprint "${blueprintName}" created!`).should('be.visible')
      cy.findByTestId('blueprint-list').contains(blueprintName).should('be.visible')
   })

   it('adds 3d model interpretation', () => {
      cy.authorize()
      cy.findByRole('button', { name: /blueprints/i }).click()
      cy.findByTestId('blueprint-list')
         .contains(/old curved sword/i)
         .click()
      cy.findByRole('button', { name: /add interpretation/i }).click()

      cy.findByTestId('interpretation-select').click()
      cy.findByTestId('interpretation-select').findByText('3d-model').click()
      cy.findByTestId('interpretation-select').click()
      cy.findByTestId('interpretation-select').findByText('weapon-sword').click()
      cy.findByTestId('interpretation-select').click()
      cy.findByTestId('interpretation-select').findByText('fire-effect').click()
      cy.findByTestId('interpretation-file-upload').selectFile(
         'cypress/fixtures/fire-sword-3d-model.glb',
         {
            action: 'drag-drop',
         }
      )
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(1000)
      cy.findByTestId('modal')
         .findByRole('button', { name: /add interpretation/i })
         .click()

      cy.findByText('Interpretation created!').should('be.visible')
      cy.findByTestId('interpretation-list')
         .contains(['3d-model', 'fire-effect', 'weapon-sword'].join(''))
         .should('be.visible')
   })
})

export {}
