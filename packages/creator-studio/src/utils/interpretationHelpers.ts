import { InterpretationPattern, TagMetadata } from '@asylum-ui/connection-library'

import { InterpretationUnwrapped } from 'types'
import { findEditableFields } from 'utils'

export type InterpretationViewType = 'image' | '3d-model' | 'unknown' | 'empty'

export const defineInterpretationType = (
   interpretation: InterpretationUnwrapped | InterpretationPattern
): InterpretationViewType => {
   if ('id' in interpretation && interpretation.interpretationInfo.src === undefined) {
      return 'empty'
   }

   const imageTags = ['png', 'jpg', 'jpeg']

   const tags =
      'id' in interpretation
         ? interpretation.pendingTags
            ? [...interpretation.tags, ...interpretation.pendingTags]
            : interpretation.tags
         : interpretation.tags ?? []

   switch (true) {
      case imageTags.some((imgTag) => tags.includes(imgTag)):
         return 'image'
      case tags.includes('3d-model'):
         return '3d-model'
      default:
         return 'unknown'
   }
}

export const buildInterpretationInitialValues = (
   interpretation: InterpretationPattern,
   tags: TagMetadata[]
) => {
   const initialTags =
      interpretation.tags && tags
         ? (interpretation.tags
              .map((tagName) => tags.find((tag) => tag.id === tagName))
              .filter((tag) => tag !== undefined) as TagMetadata[])
         : []
   const initialEditableTagFields = findEditableFields(initialTags).reduce(
      (prev, field) => ({
         ...prev,
         [field.name]:
            interpretation.fields?.find((patternField) => patternField.name === field.name)
               ?.value ??
            field.defaultValue ??
            '',
      }),
      {}
   )
   return {
      tags: initialTags,
      src: undefined,
      editableTagFields: initialEditableTagFields,
   }
}

export const compareByTags = <T extends { tags: string[] }>(a: T, b: T) =>
   a.tags.includes('default-view')
      ? -1
      : a.tags.includes('3d-model') && !b.tags.includes('default-view')
      ? -1
      : 0
