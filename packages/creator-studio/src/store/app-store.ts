import { makeAutoObservable } from 'mobx'

import { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import { INetwork, SpaceWithMetadataUnwrapped } from 'types'

class AppStore {
   account: InjectedAccountWithMeta | null = null
   selectedSpace: SpaceWithMetadataUnwrapped | null = null
   network: INetwork | null = null
   isInterfaceOpen: boolean = true
   isSpaceRunning: boolean = false
   isConnected: boolean = false
   unityContext: any = null

   constructor() {
      makeAutoObservable(this)
   }

   clear() {
      this.selectedSpace = null
      this.network = null
      this.isConnected = false
      this.unityContext = null
   }

   setAccount(account: InjectedAccountWithMeta | null) {
      this.account = account
   }

   setSelectedSpace(space: SpaceWithMetadataUnwrapped | null) {
      this.selectedSpace = space
   }

   setNetwork(network: INetwork | null) {
      this.network = network
   }

   setIsConnected(isConnected: boolean) {
      this.isConnected = isConnected
   }

   setIsInterfaceOpen(isInterfaceOpen: boolean) {
      this.isInterfaceOpen = isInterfaceOpen
   }

   setIsSpaceRunning(isSpaceRunning: boolean) {
      this.isSpaceRunning = isSpaceRunning
   }

   setUnityContext(unityContext: any) {
      this.unityContext = unityContext
   }
}

export type IAppStore = typeof AppStore.prototype
export default new AppStore()
