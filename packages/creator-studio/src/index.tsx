import { StrictMode } from 'react'

import { App } from 'App'
import 'highlight.js/styles/github-dark.css'
import { createRoot } from 'react-dom/client'
import { QueryClient, QueryClientProvider } from 'react-query'
import { BrowserRouter } from 'react-router-dom'

import { AsylumApiProvider } from 'context/api-provider'
import { StoreProvider } from 'store'
import { ScrollToTop } from 'utils/scroll-to-top'

import './index.css'
import reportWebVitals from './reportWebVitals'

const queryClient = new QueryClient()

const root = createRoot(document.getElementById('root') as HTMLDivElement)

root.render(
   <StrictMode>
      <QueryClientProvider client={queryClient}>
         <BrowserRouter>
            <ScrollToTop />
            <StoreProvider>
               <AsylumApiProvider>
                  <App />
               </AsylumApiProvider>
            </StoreProvider>
         </BrowserRouter>
      </QueryClientProvider>
   </StrictMode>
)

reportWebVitals()
