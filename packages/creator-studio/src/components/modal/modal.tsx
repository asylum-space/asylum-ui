import { useState } from 'react'

import classNames from 'classnames'

import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { FullScreenOverlay } from 'components/modal'
import { HeadingLg } from 'components/text'
import { ColorContext } from 'context'
import { ComponentProps } from 'types'

export interface ModalProps {
   title?: string
   open?: boolean
   dataTestId?: string
   onClose?: () => void
   maxWidth?: 'md' | 'lg' | 'xl' | '2xl' | 'full'
}

/**
 * Generic controlled modal windows
 * @constructor
 */
export const Modal = ({
   title,
   open = false,
   onClose,
   maxWidth = 'xl',
   children,
   dataTestId = 'modal',
}: ComponentProps<ModalProps>) => {
   const [isClosing, setIsClosing] = useState(false)

   const handleClose = () => {
      setIsClosing(true)
   }

   const handleAnimationEnd = () => {
      if (isClosing) {
         setIsClosing(false)
         onClose && onClose()
      }
   }
   return (
      <FullScreenOverlay
         open={open}
         isClosing={isClosing}
         handleAnimationEnd={handleAnimationEnd}
         handleClose={handleClose}
      >
         <ColorContext.Provider value="gray">
            <div
               className={classNames(
                  'bg-neutral-900 rounded text-white w-full cursor-auto mt-auto mb-auto',
                  {
                     'max-w-md': maxWidth === 'md',
                     'max-w-lg': maxWidth === 'lg',
                     'max-w-xl': maxWidth === 'xl',
                     'max-w-2xl': maxWidth === '2xl',
                     'max-w-full': maxWidth === 'full',
                  }
               )}
               onClick={(e) => e.stopPropagation()}
               data-testid={dataTestId}
            >
               <div className="flex items-center justify-between bg-neutral-800 px-8 py-4 rounded-t">
                  {title && <HeadingLg className="text-white">{title}</HeadingLg>}{' '}
                  <div onClick={handleClose} className="group cursor-pointer p-2 relative -right-1">
                     <CloseIcon className="fill-neutral-500 group-hover:fill-white transition-colors" />
                  </div>
               </div>
               <div className="p-4">{children}</div>
            </div>
         </ColorContext.Provider>
      </FullScreenOverlay>
   )
}
