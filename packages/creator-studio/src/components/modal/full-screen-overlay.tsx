import { useEffect } from 'react'

import classNames from 'classnames'
import ReactDOM from 'react-dom'

import { ComponentProps } from 'types'

interface FullscreenOverlayProps {
   handleAnimationEnd: () => void
   handleClose: () => void
   open: boolean
   isClosing: boolean
   inPortal?: boolean
}

/**
 * Backing for any modal elements
 * @constructor
 */
export const FullScreenOverlay = ({
   children,
   handleAnimationEnd,
   handleClose,
   open,
   isClosing,
   inPortal = true,
}: ComponentProps<FullscreenOverlayProps>) => {
   const root = document.getElementById('modal-root')
   useEffect(() => {
      document.body.style.overflow = open ? 'hidden' : ''
   }, [open])

   if (!(root instanceof HTMLElement)) return null

   const Overlay = (
      <div
         className={classNames(
            'fixed inset-0 z-30 flex items-center justify-center bg-gradient-backdrop cursor-pointer backdrop-blur-sm overflow-auto py-4',
            open && !isClosing ? 'animate-fade-in' : 'animate-fade-out'
         )}
         onAnimationEnd={handleAnimationEnd}
         onClick={handleClose}
      >
         {children}
      </div>
   )

   return inPortal ? ReactDOM.createPortal(open ? Overlay : null, root) : Overlay
}
