// TODO: Make Typescript understand type narrowing
import React from 'react'

import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { Paragraph } from 'components/text'
import { ComponentProps } from 'types'

import { InputLabel } from './input-label'

interface InputFieldProps<T extends 'input' | 'textarea'> {
   /**
    * 'type' field from native input
    */
   as: T
   /**
    * adds label above the input
    */
   label?: string
   /**
    * adds hoverable tooltip icon near label.
    */
   labelTooltipMessage?: string
   /**
    * error message to be displayed near the input
    */
   errorMessage?: string | false
   /**
    * shows react element at the end of the input area
    */
   endAdornment?: React.ReactNode
   /**
    * classes to be injected in the input element
    */
   inputClassName?: string
}

/**
 * Styled input field for various forms.
 * @constructor
 */
export const InputField = <T extends 'input' | 'textarea'>({
   as,
   label,
   labelTooltipMessage,
   endAdornment,
   errorMessage,
   disabled,
   className,
   inputClassName,
   name,
   ...rest
}: ComponentProps<InputFieldProps<T>, T>) => {
   const props = {
      className: twMerge(
         classNames(
            'peer text-base font-primary w-full bg-neutral-700 rounded px-4 py-3 outline-none outline-2 outline-offset-0 placeholder:text-neutral-400 transition-all',
            {
               'pr-12': endAdornment,
               'cursor-default pointer-events-none bg-neutral-800 text-neutral-500': disabled,
            },
            errorMessage ? 'outline-red-400' : 'outline-none focus:outline-neutral-600'
         ),
         inputClassName
      ),
      ...(disabled && { tabIndex: -1 }),
   }
   return (
      <>
         <div
            className={twMerge(classNames('w-full relative flex flex-col items-start'), className)}
         >
            {label && (
               <InputLabel tooltipMessage={labelTooltipMessage} htmlFor={name}>
                  {label}
               </InputLabel>
            )}
            {as === 'input' && (
               <input id={name} {...props} {...(rest as React.ComponentPropsWithoutRef<'input'>)} />
            )}
            {as === 'textarea' && (
               <textarea
                  id={name}
                  {...props}
                  {...(rest as React.ComponentPropsWithoutRef<'textarea'>)}
               />
            )}

            <div className="absolute right-5 top-1/2 -translate-y-1/2 opacity-50 focus:opacity-100 peer-focus:opacity-100 hover:opacity-100 peer-hover:opacity-100 transition-all">
               {endAdornment}
            </div>
            {errorMessage && (
               <Paragraph className="text-red-300 ml-2 mt-2">{errorMessage}</Paragraph>
            )}
         </div>
      </>
   )
}
