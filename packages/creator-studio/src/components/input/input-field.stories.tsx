import { ComponentMeta, ComponentStory } from '@storybook/react'

import { InputField, InputFieldSkeleton } from './index'

export default {
   title: 'InputField',
   component: InputField,
   decorators: [
      (Story) => (
         <div className="p-20 bg-neutral-800">
            <Story />
         </div>
      ),
   ],
} as ComponentMeta<typeof InputField>

const Template: ComponentStory<typeof InputField> = (args) => <InputField {...args} />

export const Primary = Template.bind({})

export const Skeleton = () => (
   <InputFieldSkeleton label="Input field label" labelTooltipMessage="tooltip" />
)

Primary.args = {
   disabled: false,
   label: 'Input field label',
   as: 'input',
   placeholder: 'type here...',
   labelTooltipMessage: 'tooltip message',
}
