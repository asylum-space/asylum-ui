import { ChangeEvent, useEffect, useState } from 'react'

import axios from 'axios'
import classNames from 'classnames'
import { uniqueId } from 'lodash/fp'
import { twMerge } from 'tailwind-merge'

import { CID } from '@asylum-ui/connection-library'

import { ReactComponent as IpfsIcon } from 'assets/svg/ipfs.svg'
import { Paragraph } from 'components/text'
import { ComponentProps } from 'types'
import { ipfsPath } from 'utils'

import { InputLabel } from './input-label'

interface InputFileUploadProps {
   name: string
   label?: string
   value?: CID
   errorMessage?: string
   onChange?: (file: File) => void
   onLoad?: (buffer: ArrayBuffer) => void
   dataTestId?: string
}

/**
 * File uploader which supports drop gestures and shows uploaded file, and it's name as background if possible.
 * @constructor
 */
export const InputFileUpload = ({
   name,
   label,
   value,
   onChange,
   onLoad,
   errorMessage,
   className,
   dataTestId = 'input-file-upload',
   ...rest
}: ComponentProps<InputFileUploadProps, 'input'>) => {
   const [inputId] = useState(uniqueId(name))
   const [fileType, setFileType] = useState<string | null>(null)
   const [fileName, setFileName] = useState<string | null>(null)

   useEffect(() => {
      if (!value) return
      axios.get(ipfsPath(value)).then((response) => setFileType(response.headers['content-type']))
      !fileName && setFileName(value)
   }, [value])

   const handleUpload = (file: File) => {
      const readerArrayBuffer = new FileReader()
      onChange && onChange(file)
      setFileName(file.name)

      readerArrayBuffer.addEventListener('load', () => {
         onLoad && onLoad(readerArrayBuffer.result as ArrayBuffer)
      })
      readerArrayBuffer.readAsArrayBuffer(file)
   }

   const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
      if (event.target.files) {
         const file = event.target.files[0]
         handleUpload(file)
      }
   }

   const isImage = fileType !== null && fileType.startsWith('image')

   return (
      <div>
         <div
            className={twMerge(classNames('w-full relative'), className)}
            onDragOver={(e) => {
               e.preventDefault()
            }}
            onDrop={(event) => {
               event.preventDefault()
               event.stopPropagation()
               const file = event.dataTransfer.files[0]
               handleUpload(file)
            }}
            data-testid={dataTestId}
         >
            {label && <InputLabel className="mb-2">{label}</InputLabel>}
            <label
               style={isImage ? { backgroundImage: `url(${ipfsPath(value)})` } : {}}
               htmlFor={`${inputId}-file-upload`}
               className={classNames(
                  'block border-dashed border-2 cursor-pointer rounded bg-cover overflow-hidden bg-center',
                  errorMessage ? 'border-red-500' : 'border-white'
               )}
            >
               <div
                  className={classNames(
                     'relative flex flex-col gap-1 py-12 items-center backdrop-grayscale backdrop-blur',
                     fileType && !isImage && 'bg-gradient-backdrop',
                     !fileType && 'bg-neutral-900'
                  )}
               >
                  {isImage && (
                     <>
                        <img src={ipfsPath(value)} alt="" className="absolute h-full p-2 top-0" />
                        <div className="absolute inset-0 bg-neutral-900 opacity-70" />
                     </>
                  )}
                  <IpfsIcon className="fill-white w-9 h-9 mb-4 z-10" />
                  <Paragraph className="z-10">
                     {!value ? 'Drag an interpretation source here' : fileName}
                  </Paragraph>
                  <Paragraph className="z-10">
                     {!value && 'or '}
                     <span className="text-asylum-blue underline">Browse Files</span>
                  </Paragraph>
               </div>
            </label>
            <input
               id={`${inputId}-file-upload`}
               name={name}
               type="file"
               className="hidden"
               onChange={handleChange}
               {...rest}
            />
         </div>
         {errorMessage && <Paragraph className="text-red-400 ml-2 mt-2">{errorMessage}</Paragraph>}
      </div>
   )
}
