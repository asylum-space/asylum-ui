import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ReactComponent as Info } from 'assets/svg/info.svg'
import { Tooltip } from 'components/tooltip'
import { ComponentProps } from 'types'

interface InputLabelProps {
   tooltipMessage?: string
}

/**
 * Label for Input field
 * @see InputField
 * @alpha
 * @constructor
 */
export const InputLabel = ({
   htmlFor,
   className,
   children,
   tooltipMessage,
}: ComponentProps<InputLabelProps, 'label'>) => (
   <label
      htmlFor={htmlFor}
      className={twMerge(
         classNames('font-secondary flex gap-2 items-center text-lg mb-1'),
         className
      )}
   >
      {children}
      {tooltipMessage !== undefined && (
         <Tooltip text={tooltipMessage}>
            <Info className="w-4 h-4 fill-neutral-500 hover:fill-white transition-colors duration-75 cursor-pointer" />
         </Tooltip>
      )}
   </label>
)
