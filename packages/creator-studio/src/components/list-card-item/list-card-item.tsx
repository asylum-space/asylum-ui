import { Link } from 'react-router-dom'

import { Button } from 'components/button'
import { Card } from 'components/card'
import { HeadingLg, Paragraph } from 'components/text'
import { InterpretationRepresentation } from 'modules/overview/overview-card/interpretation-representation'
import { ComponentProps, InterpretationUnwrapped } from 'types'
import { formatAddress } from 'utils'

interface ListCardItemProps {
   /**
    * link, passed to react-router Link element.
    */
   to: string
   /**
    * title of the card
    */
   title: string
   id: string
   /**
    * interpretation to be displayed
    */
   interpretation: InterpretationUnwrapped
   description: string
   /**
    * related blockchain address
    */
   address: string
   /**
    * action fired when in-card button pressed, if no action provided, button vanishes
    */
   actionButton?: {
      onClick: (id: string) => void
      text: string
   }
   /**
    * if true 'pending' message displayed
    */
   pending?: boolean
}

/**
 * Card-link which represents standalone interpretation or blueprint/item with default interpretation
 * @constructor
 */
export const ListCardItem = ({
   to,
   title,
   id,
   interpretation,
   address,
   description,
   actionButton,
   pending = false,
}: ComponentProps<ListCardItemProps>) => {
   return (
      <Link to={to} className="flex flex-col" data-testid="list-card-item">
         <Card
            header={<HeadingLg className="line-clamp-2">{title}</HeadingLg>}
            className="cursor-pointer grow hover:border-glitch-effect"
            headerClassName="p-5"
            contentClassName="flex flex-col gap-4 px-5"
         >
            <InterpretationRepresentation
               className="aspect-square grow"
               isPending={pending}
               interpretation={interpretation}
            />
            <Paragraph className="mt-2 group-hover:text-white line-clamp-2 min-h-[2em]">
               {description}
            </Paragraph>
            <div className="flex flex-row justify-between items-center">
               <Paragraph className="group-hover:text-white">{formatAddress(address)}</Paragraph>
               {actionButton && (
                  <Button
                     className="before:hidden px-3 py-1.5 hover:text-gray-700"
                     onClick={(e) => {
                        e.preventDefault()
                        actionButton.onClick(id)
                     }}
                  >
                     {actionButton.text}
                  </Button>
               )}
            </div>
         </Card>
      </Link>
   )
}
