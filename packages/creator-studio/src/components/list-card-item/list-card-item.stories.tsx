import { ComponentMeta } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'

import empty from '../../assets/svg/empty.svg'
import { InterpretationUnwrapped } from 'types'

import { ListCardItem, ListCardItemSkeleton } from './index'

const mockInterpretation: InterpretationUnwrapped = {
   tags: ['tag1', 'tag2', 'tag3'],
   id: '1:2',
   interpretationInfo: { src: empty, metadata: { property: 'value' } },
   pending: false,
   pendingTags: [],
   pendingRemoval: false,
}

const mockAddress = '5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn'

export default {
   title: 'ListCardItem',
   component: ListCardItem,
} as ComponentMeta<typeof ListCardItem>

export const Primary = () => (
   <BrowserRouter>
      <div className="w-60 m-auto">
         <ListCardItem
            to=""
            title="item title"
            id="1:2"
            interpretation={mockInterpretation}
            description="Card description"
            address={mockAddress}
         />
      </div>
   </BrowserRouter>
)

export const Skeleton = () => (
   <div className="w-60 m-auto bg-neutral-500 ">
      <ListCardItemSkeleton />
   </div>
)
