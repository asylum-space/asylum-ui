import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

export const HeadingLg = ({ className, children }: ComponentProps<{}, 'h2'>) => {
   return (
      <h2 className={twMerge(classNames('font-normal text-xl text-inherit'), className)}>
         {children}
      </h2>
   )
}
