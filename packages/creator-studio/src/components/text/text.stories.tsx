import { ComponentMeta } from '@storybook/react'

import { Heading, HeadingLg, HeadingXl, Paragraph } from './index'

const mockText =
   'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet maiores sed sit tempora vitae\n' +
   '   voluptatibus. Adipisci assumenda beatae, corporis cumque dolor doloribus esse, molestias nam nihil, nobis\n' +
   '   perspiciatis ut vero.'

export default {
   title: 'Text',
   component: Paragraph,
} as ComponentMeta<typeof Paragraph>

export const ParagraphStory = () => <Paragraph>{mockText}</Paragraph>
export const HeadingStory = () => <Heading>{mockText}</Heading>
export const HeadingLgStory = () => <HeadingLg>{mockText}</HeadingLg>
export const HeadingXlStory = () => <HeadingXl>{mockText}</HeadingXl>
