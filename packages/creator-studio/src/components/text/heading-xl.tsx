import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

export const HeadingXl = ({ className, children }: ComponentProps<{}, 'h1'>) => {
   return (
      <h1 className={twMerge(classNames('font-medium text-2xl text-inherit'), className)}>
         {children}
      </h1>
   )
}
