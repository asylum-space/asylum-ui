import { ComponentMeta, ComponentStory } from '@storybook/react'

import { ThreeViewer } from './three-viewer'

const threeModel = require('assets/mocks/sword-3d.glb')

export default {
   title: 'ThreeViewer',
   component: ThreeViewer,
} as ComponentMeta<typeof ThreeViewer>

const Template: ComponentStory<typeof ThreeViewer> = (args) => <ThreeViewer {...args} />

export const Primary = Template.bind({})

Primary.args = {
   src: threeModel,
}
