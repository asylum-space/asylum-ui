import { ComponentMeta, ComponentStory } from '@storybook/react'

import { Tag, TagGroup } from './index'

export default {
   title: 'TagGroup',
   component: TagGroup,
   subcomponents: { Tag },
} as ComponentMeta<typeof TagGroup>

const Template: ComponentStory<typeof TagGroup> = (args) => <TagGroup {...args} />

export const Primary = Template.bind({})

Primary.args = {
   tags: ['tag 1', 'tag 2', 'tag 3'],
}
