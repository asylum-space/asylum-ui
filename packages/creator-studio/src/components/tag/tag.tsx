import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

interface TagProps {
   /**
    * deterministic form of the tag element
    */
   form?: 'leaf' | 'pill'
}

/**
 * Simple styled string for tag. May be in two predefined forms.
 * @see TagGroup
 * @constructor
 */
export const Tag = ({ className, form = 'leaf', children }: ComponentProps<TagProps, 'span'>) => (
   <span
      className={twMerge(
         classNames('py-0.5 whitespace-nowrap text-lg bg-asylum-blue', {
            'rounded px-3': form === 'pill',
            'rounded-tl-2xl rounded-br-2xl px-3': form === 'leaf',
         }),
         className
      )}
   >
      {children}
   </span>
)
