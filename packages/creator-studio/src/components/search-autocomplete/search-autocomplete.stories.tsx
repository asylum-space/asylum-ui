import { ComponentMeta, ComponentStory } from '@storybook/react'

import { SearchAutocomplete } from './search-autocomplete'

export default {
   title: 'SearchAutocomplete',
   component: SearchAutocomplete,
} as ComponentMeta<typeof SearchAutocomplete>

const Template: ComponentStory<typeof SearchAutocomplete> = (args) => (
   <SearchAutocomplete {...args} />
)

export const Primary = Template.bind({})

Primary.args = {
   className: 'my-32 mx-4 relative',
   variants: ['blue', 'green', 'red'],
   onVariantSelect: () => null,
   value: '',
}
