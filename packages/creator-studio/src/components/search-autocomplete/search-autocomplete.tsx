import React, { useState } from 'react'

import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { ReactComponent as SearchIcon } from 'assets/svg/search.svg'
import { InputField } from 'components/input'
import { Paragraph } from 'components/text'
import { ComponentProps } from 'types'

interface SearchAutocompleteProps {
   /**
    * callback when value selected
    * @param value
    */
   onVariantSelect: (value: string) => void
   /**
    * selected value
    */
   value?: string
   /**
    * all variants, displayed in a drop list
    */
   variants?: string[]
}

interface VariantProps {
   clickHandler: (variant: string) => void
   text: string
   userVariant?: boolean
}

const Variant = ({ text, userVariant, clickHandler, className }: ComponentProps<VariantProps>) => {
   return (
      <div
         tabIndex={0}
         onMouseDown={() => {
            clickHandler(text)
         }}
         onKeyDown={(e) => e.key === 'Enter' && clickHandler(text)}
         className={classNames(
            'cursor-pointer focus-visible:outline-none hover:bg-neutral-700 bg-neutral-800',
            className
         )}
      >
         <Paragraph className="text-white font-sans px-4 py-2.5">
            {text}
            {userVariant && (
               <span className="text-gray-400 grow inline"> - search items by name</span>
            )}
         </Paragraph>
      </div>
   )
}

/**
 * Search input field.
 * Acquires variants as a property.
 * When variant selected calls callback.
 */
export const SearchAutocomplete = ({
   value = '',
   variants = [],
   onVariantSelect,
   className,
}: ComponentProps<SearchAutocompleteProps, 'input'>) => {
   const [isOpen, setIsOpen] = useState(false)
   const [inputValue, setInputValue] = useState(value)
   const [isInputFocused, setIsInputFocused] = useState(false)
   const [suggestions, setSuggestions] = useState<string[]>([])
   const filterVariants = (value: string) => {
      const filteredVariants = variants.filter((variant) =>
         variant?.toLowerCase().includes(value.toLowerCase())
      )
      setSuggestions(filteredVariants.slice(0, 10))
   }

   const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value
      filterVariants(value)
      setInputValue(value)
      setIsOpen(true)

      if (!value) {
         onVariantSelect('')
      }
   }

   const handleSelect = (variant: string) => {
      onVariantSelect(variant)
      filterVariants(variant)
      setInputValue(variant)
      setIsOpen(false)
   }

   return (
      <div
         className={twMerge(
            classNames('bg-black flex flex-col items-start relative z-10'),
            className
         )}
      >
         <InputField
            as="input"
            autoComplete="off"
            className="z-0"
            inputClassName="bg-neutral-800"
            name="search"
            value={inputValue}
            onChange={handleChange}
            placeholder="Search by blueprint id, name or owner"
            onFocus={() => setIsInputFocused(true)}
            onBlur={() => setIsInputFocused(false)}
            endAdornment={
               <div className="flex items-center gap-4">
                  {inputValue && (
                     <CloseIcon
                        className="cursor-pointer fill-neutral-500 hover:fill-white transition-all"
                        onClick={() => handleSelect('')}
                     />
                  )}
                  <SearchIcon
                     className="cursor-pointer fill-neutral-500 hover:fill-white transition-all w-6 h-6"
                     onClick={() => handleSelect(inputValue)}
                  />
               </div>
            }
            onKeyDown={(e) => e.key === 'Enter' && handleSelect(inputValue)}
         />
         <div className="w-full rounded overflow-hidden bg-gray-800 absolute top-[calc(100%_+_0.5rem)] -z-10 shadow-xl">
            {inputValue && isOpen && isInputFocused && (
               <>
                  <Variant
                     userVariant={true}
                     key="user-input"
                     text={inputValue}
                     clickHandler={handleSelect}
                  />
                  {suggestions.map((variant) => (
                     <Variant key={variant} text={variant} clickHandler={handleSelect} />
                  ))}
               </>
            )}
         </div>
      </div>
   )
}
