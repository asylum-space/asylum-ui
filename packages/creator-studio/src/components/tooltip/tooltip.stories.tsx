import { ComponentMeta, ComponentStory } from '@storybook/react'

import { Tooltip } from './tooltip'

export default {
   title: 'Tooltip',
   component: Tooltip,
} as ComponentMeta<typeof Tooltip>

const Template: ComponentStory<typeof Tooltip> = (args) => (
   <Tooltip {...args} className="w-24 flex justify-center m-auto my-12">
      <div>generic div</div>
   </Tooltip>
)

export const Primary = Template.bind({})

Primary.args = {
   text: 'Tooltip text',
}
