import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

interface TooltipProps {
   /**
    * text to be displayed when tooltip trigger hovered
    */
   text: string
}

/**
 * Wrapper for any element. Shows tooltip above element when hovered.
 * @constructor
 */
export const Tooltip = ({ children, text, className }: ComponentProps<TooltipProps>) => {
   return (
      <div className={twMerge(classNames('relative group'), className)}>
         <div className="absolute bottom-[calc(100%+1rem)] w-48 left-1/2 px-4 py-2 rounded translate-x-[-50%] bg-neutral-800 text-white text-center shadow-lg shadow-neutral-800/30 opacity-0 invisible group-hover:opacity-100 group-hover:visible transition-opacity after:content-[''] after:absolute after:translate-x-[-50%] after:top-full after:left-1/2 after:border-8 after:border-transparent after:border-t-neutral-800">
            {text}
         </div>
         {children}
      </div>
   )
}
