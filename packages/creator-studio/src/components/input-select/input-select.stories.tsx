import { ComponentMeta, ComponentStory } from '@storybook/react'

import { InputSelect } from './input-select'

const values = ['green', 'black', 'blue']

export default {
   title: 'InputSelect',
   component: InputSelect,
} as ComponentMeta<typeof InputSelect>

const Template: ComponentStory<typeof InputSelect> = (args) => (
   <InputSelect {...args} className="my-32" isMulti={true} />
)

export const Tags = Template.bind({})

Tags.args = {
   placeholder: 'select placeholder',
   name: 'select name',
   options: values.map((color) => ({ label: color, value: color })),
}
