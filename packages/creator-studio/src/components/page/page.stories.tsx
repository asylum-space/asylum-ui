import { ComponentMeta, ComponentStory } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'

import { Page } from './page'

export default {
   title: 'Page',
   component: Page,
   decorators: [
      (Story) => (
         <div className="bg-neutral-800 p-8">
            <Story />
         </div>
      ),
   ],
} as ComponentMeta<typeof Page>

const Template: ComponentStory<typeof Page> = (args) => (
   <BrowserRouter>
      <Page {...args} />
   </BrowserRouter>
)

export const Primary = Template.bind({})

Primary.args = {
   title: 'Page Title',
   children: 'Page content',
}
