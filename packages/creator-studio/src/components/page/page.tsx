import { ReactNode } from 'react'

import classNames from 'classnames'
import { useNavigate } from 'react-router-dom'
import { twMerge } from 'tailwind-merge'

import { ReactComponent as ArrowLeftIcon } from 'assets/svg/arrow-left.svg'
import { Hr } from 'components/hr'
import { HeadingXl } from 'components/text'
import { ComponentProps } from 'types'

interface PageProps {
   /**
    * Title of the page, shows at the top.
    */
   title: string
   /**
    * button in the header of the page
    */
   headerButton?: ReactNode
   /**
    * navigation arrow in the header of the page
    */
   returnButton?: boolean
}

/**
 * Generic Page, should be used as main wrapper for all pages.
 * @constructor
 */
export const Page = ({
   title,
   children,
   headerButton = null,
   returnButton = false,
   className,
}: ComponentProps<PageProps>) => {
   const navigate = useNavigate()

   return (
      <div className="container mx-auto">
         <div className="flex justify-between items-center">
            <HeadingXl className="text-white self-start flex items-center gap-2">
               {returnButton && (
                  <ArrowLeftIcon
                     className="fill-white cursor-pointer p-2 box-content"
                     onClick={() => navigate(-1)}
                  />
               )}
               {title}
            </HeadingXl>
            {headerButton}
         </div>
         <Hr className="my-10" />
         <div className={twMerge(classNames('pb-40'), className)}>{children}</div>
      </div>
   )
}
