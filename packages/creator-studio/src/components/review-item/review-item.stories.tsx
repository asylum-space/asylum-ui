import { ComponentMeta } from '@storybook/react'

import { Review } from '@asylum-ui/connection-library'

import { ReviewCard } from './review-item'

const mockReview: Review = {
   id: '1',
   name: 'reviewName',
   text: 'Review text, may be very long text!',
   rating: 3,
   address: '5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn',
}

export default {
   title: 'ReviewCard',
   component: ReviewCard,
} as ComponentMeta<typeof ReviewCard>

export const Primary = () => <ReviewCard review={mockReview} />
