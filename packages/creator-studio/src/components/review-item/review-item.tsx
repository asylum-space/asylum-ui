import { Review } from '@asylum-ui/connection-library'

import { Avatar } from 'components/avatar'
import { Card } from 'components/card'
import { RatingStarsComponent } from 'components/rating-star'
import { Paragraph } from 'components/text'
import { ComponentProps } from 'types'
import { formatAddress } from 'utils'

interface ReviewCardProps {
   /**
    * review of the space.
    */
   review: Review
}

/**
 * Rendered review from the node.
 * @constructor
 */
export const ReviewCard = ({ review }: ComponentProps<ReviewCardProps>) => {
   return (
      <Card
         header={
            <>
               <div className="flex items-center">
                  <Avatar />
                  <div className="pl-4">
                     <Paragraph>{formatAddress(review.address)}</Paragraph>
                  </div>
               </div>
               <div className="flex justify-center items-center">
                  <RatingStarsComponent rating={review.rating} />
               </div>
            </>
         }
         headerClassName="flex justify-between"
      >
         <div className="flex flex-col justify-between gap-5">
            <Paragraph>{review.text}</Paragraph>
         </div>
      </Card>
   )
}
