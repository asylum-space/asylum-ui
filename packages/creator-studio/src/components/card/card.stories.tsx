import { ComponentMeta, ComponentStory } from '@storybook/react'

import { Card } from './card'

export default {
   title: 'Card',
   component: Card,
} as ComponentMeta<typeof Card>

const Template: ComponentStory<typeof Card> = (args) => <Card {...args} />

export const Primary = Template.bind({})

Primary.args = {
   header: 'Header content',
   children: 'Main content, usually long text and/or jsx components',
}
