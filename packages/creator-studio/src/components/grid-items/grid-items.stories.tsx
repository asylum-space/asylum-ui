import { ComponentMeta } from '@storybook/react'

import logo from 'assets/svg/empty.svg'

import { GridItems } from './grid-items'

const Image = <img src={logo} alt="logo" />

export default {
   title: 'GridItems',
   component: GridItems,
} as ComponentMeta<typeof GridItems>

export const Primary = () => (
   <GridItems>
      <>{[...Array(10)].map(() => Image)}</>
   </GridItems>
)
