import { ComponentMeta, ComponentStory } from '@storybook/react'

import { RatingStarsComponent } from './rating-star'

export default {
   title: 'RatingStarsComponent',
   component: RatingStarsComponent,
} as ComponentMeta<typeof RatingStarsComponent>

const Template: ComponentStory<typeof RatingStarsComponent> = (args) => (
   <RatingStarsComponent {...args} />
)

export const Primary = Template.bind({})

Primary.args = {
   rating: 4,
}
