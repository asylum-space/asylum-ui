import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import 'react-toastify/dist/ReactToastify.css'

import { Toast } from 'components/toast'
import { WalletConnect } from 'modules/wallet-connect'
import { useStore } from 'store'
import { ComponentProps } from 'types'

import { Header } from './header'
import { Sidebar } from './sidebar'

export const Layout = observer(({ children }: ComponentProps) => {
   const store = useStore()
   return (
      <div
         className={classNames('relative overflow-hidden', {
            'animate-slide-out': !store.isInterfaceOpen && store.isSpaceRunning,
            'animate-slide-in': store.isInterfaceOpen && store.isSpaceRunning,
         })}
      >
         <Sidebar />
         <main className="py-8 px-12 h-screen overflow-auto md:ml-52 lg:ml-72">
            <Header>
               <WalletConnect />
            </Header>
            <Toast className="md:left-28 lg:left-36" />
            {children}
         </main>
      </div>
   )
})
