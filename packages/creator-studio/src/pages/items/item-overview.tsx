import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'

import { SubmittableResult } from '@polkadot/api'
import { acceptItemUpdate, fetchItem } from 'api'
import { Page } from 'components/page'
import { Overview } from 'modules/overview'

export const ItemOverview = () => {
   const { id } = useParams()
   if (!id) return null

   const [blueprintId, itemId] = id.split('_')
   const queryClient = useQueryClient()

   const { data: item } = useQuery(['item', [blueprintId, itemId]], () =>
      fetchItem(blueprintId, itemId)
   )

   const acceptItemUpdateMutation = useMutation(
      (): Promise<SubmittableResult> => acceptItemUpdate(blueprintId, itemId),
      {
         onSuccess: () => {
            queryClient.invalidateQueries(['blueprints', blueprintId])
         },
      }
   )

   if (!item) return null

   return (
      <Page title="Item overview" returnButton={true}>
         <Overview
            type="item"
            data={item}
            onAcceptItemUpdateClick={() => acceptItemUpdateMutation.mutate()}
         />
      </Page>
   )
}
