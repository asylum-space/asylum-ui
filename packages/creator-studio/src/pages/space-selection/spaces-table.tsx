import classNames from 'classnames'
import { observer } from 'mobx-react-lite'

import { GridItems } from 'components/grid-items'
import { SpaceCard } from 'pages/space-selection/space-card'
import { useStore } from 'store'
import { SpaceWithMetadataUnwrapped } from 'types'
import { ipfsPath } from 'utils'

interface Props {
   spaces: SpaceWithMetadataUnwrapped[]
}

export const SpacesTable = observer(({ spaces }: Props) => {
   const store = useStore()
   const handleSelectSpace = (space: SpaceWithMetadataUnwrapped) => {
      store.setSelectedSpace(space)
   }

   return (
      <GridItems>
         {spaces &&
            spaces.map((item: SpaceWithMetadataUnwrapped) => {
               return (
                  <SpaceCard
                     className={classNames({
                        grayscale: store.selectedSpace && item.id !== store.selectedSpace?.id,
                     })}
                     active={item.id === store.selectedSpace?.id}
                     key={item.title}
                     id={item.id}
                     disabled={store.isSpaceRunning}
                     onClick={() => handleSelectSpace(item)}
                     title={item.title}
                     img={ipfsPath(item.img)}
                  />
               )
            })}
      </GridItems>
   )
})
