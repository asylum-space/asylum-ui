import { useEffect } from 'react'

import { sortBy } from 'lodash/fp'
import { observer } from 'mobx-react-lite'
import { useQuery, useQueryClient } from 'react-query'

import { fetchSpaces } from 'api'
import { ReactComponent as RefreshIcon } from 'assets/svg/refresh.svg'
import { Button } from 'components/button'
import { Page } from 'components/page'
import { Paragraph } from 'components/text'
import { SpacesTable } from 'pages/space-selection/spaces-table'
import { useStore } from 'store'

interface IRefreshButton {
   onClick: () => void
}

const RefreshButton = ({ onClick }: IRefreshButton) => (
   <Button variant="light" className="pl-4 w-32" onClick={onClick}>
      <RefreshIcon className="fill-text-base w-4 h-4 inline-block mr-2" />
      Refresh
   </Button>
)

export const SpaceSelection = observer(() => {
   const store = useStore()
   const queryClient = useQueryClient()
   const { data: spaces, refetch } = useQuery(['spaces', store.account?.address], () =>
      store.account ? fetchSpaces(store.account) : null
   )

   useEffect(() => {
      queryClient.invalidateQueries('spaces')
   }, [store.account])

   return (
      <Page title="Select space" headerButton={<RefreshButton onClick={refetch} />}>
         {spaces?.length ? (
            <SpacesTable spaces={sortBy('id', spaces)} />
         ) : (
            <Paragraph className="text-white">
               No spaces found associated with your account.
            </Paragraph>
         )}
      </Page>
   )
})
