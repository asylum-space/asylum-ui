import classNames from 'classnames'

import { Heading } from 'components/text'
import { Tooltip } from 'components/tooltip'
import { ComponentProps } from 'types'

interface Props {
   title: string
   img: string
   id: string
   active?: boolean
   disabled?: boolean
   onClick: (id: string) => void
}

export const SpaceCard = ({
   active = false,
   title,
   img,
   onClick,
   id,
   disabled = false,
   className,
}: ComponentProps<Props>) => {
   const clickHandler = () => {
      if (disabled) return
      onClick(id)
   }

   const content = (
      <div
         className={classNames([
            'group flex flex-col bg-neutral-800 rounded transition-all duration-400',
            {
               'bg-neutral-700': active,
               'border-glitch-effect cursor-pointer': !active && !disabled,
               'brightness-50': !active && disabled,
            },
            className,
         ])}
         onClick={clickHandler}
      >
         <Heading className="text-center text-[1.25rem] py-2">{title}</Heading>
         <div
            style={{ backgroundImage: img ? `url('${img}')` : '' }}
            className="w-full aspect-square bg-cover bg-center"
         />
      </div>
   )

   return disabled && !active ? (
      <Tooltip text="Close the space before selecting one">{content}</Tooltip>
   ) : (
      content
   )
}
