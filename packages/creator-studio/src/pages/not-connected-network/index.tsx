import { ReactComponent as DisconnectedIcon } from 'assets/svg/disconnected.svg'
import { Paragraph } from 'components/text'

export const NotConnectedNetwork = () => {
   return (
      <div className="flex flex-col items-center justify-center m-auto h-[80%]">
         <DisconnectedIcon className="w-24 h-24 mb-6" />
         <Paragraph className="text-white text-center mb-6">
            Please, select endpoint to connect
         </Paragraph>
      </div>
   )
}
