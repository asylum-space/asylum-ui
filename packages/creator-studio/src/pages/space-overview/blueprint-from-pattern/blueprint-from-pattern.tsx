import { useParams, useSearchParams } from 'react-router-dom'

import { Page } from 'components/page'
import { ExtendBlueprintForPattern } from 'pages/space-overview/blueprint-from-pattern/extend-blueprint-from-pattern/extend-blueprint-for-pattern'
import { NewBlueprintFromPattern } from 'pages/space-overview/blueprint-from-pattern/new-blueprint-from-pattern/new-blueprint-from-pattern'
import { useStore } from 'store'

export const BlueprintFromPattern = () => {
   const { patternName } = useParams()
   const [params] = useSearchParams()
   const paramsObj = Object.fromEntries(params) as unknown as { extend: boolean }
   const { selectedSpace } = useStore()

   const interpretations = selectedSpace?.patterns?.find(
      (pattern) => pattern.name === patternName
   )?.interpretations

   if (!interpretations || !patternName) return null

   return (
      <div className="max-w-4xl m-auto">
         <Page
            title={paramsObj.extend ? 'Adapt existing blueprint' : 'Create blueprint'}
            className="m-auto flex flex-col gap-10"
         >
            {paramsObj.extend ? (
               <ExtendBlueprintForPattern interpretations={interpretations} />
            ) : (
               <NewBlueprintFromPattern
                  patternName={patternName}
                  interpretations={interpretations}
               />
            )}
         </Page>
      </div>
   )
}
