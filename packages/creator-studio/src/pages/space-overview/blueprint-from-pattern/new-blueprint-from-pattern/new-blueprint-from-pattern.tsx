import { useState } from 'react'

import { FormikProps } from 'formik'
import { isNil } from 'lodash'
import { useQueryClient } from 'react-query'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'

import {
   AsylumApi,
   BlueprintCreate,
   InterpretationCreate,
   InterpretationPattern,
} from '@asylum-ui/connection-library'

import { Button } from 'components/button'
import { Card } from 'components/card'
import { HeadingLg } from 'components/text'
import { BlueprintForm, BlueprintFormValues } from 'modules/form/blueprint-form'
import { InterpretationFormValues } from 'modules/form/interpretation-form'
import { InterpretationFormCard } from 'pages/space-overview/blueprint-from-pattern/components/interpretation-form-card'
import { touchAndValidate } from 'utils'

interface NewBlueprintFromPatternProps {
   patternName: string
   interpretations: InterpretationPattern[]
}

export const NewBlueprintFromPattern = ({ patternName, interpretations }: NewBlueprintFromPatternProps) => {
   const queryClient = useQueryClient()
   const navigate = useNavigate()

   const [blueprintFormik, setBlueprintFormik] = useState<FormikProps<BlueprintFormValues> | null>(
      null
   )
   // eslint-disable-next-line func-call-spacing
   const [interpretationFormik, setInterpretationFormik] = useState<
      (FormikProps<InterpretationFormValues> | null)[]
   >([null])

   const interpretationFormikSetter = (
      formik: FormikProps<InterpretationFormValues>,
      index: number
   ) => {
      setInterpretationFormik((prevState) => {
         const newState = [...prevState]
         newState[index] = formik
         return newState
      })
   }

   const [isSubmitting, setIsSubmitting] = useState(false)

   const formikArray = [blueprintFormik, ...interpretationFormik]

   const handleSubmit = async () => {
      if (formikArray.some(isNil)) return

      formikArray.forEach((formik) => touchAndValidate(formik as FormikProps<any>))
      if (formikArray.some((formik) => !formik?.isValid)) return

      try {
         setIsSubmitting(true)
         const blueprint = (await blueprintFormik!.submitForm()) as BlueprintCreate | undefined
         const interpretations = (await Promise.all(
            interpretationFormik.map((formik) => formik!.submitForm())
         )) as (InterpretationCreate | undefined)[]

         if (interpretations.some(isNil) || isNil(blueprint)) return

         await AsylumApi.createBlueprint(
            blueprint.name,
            blueprint.metadata,
            100,
            interpretations as InterpretationCreate[]
         )

         queryClient.invalidateQueries('blueprints')

         navigate('/blueprints')
      } catch (error) {
         toast.error(error as string)
      } finally {
         setIsSubmitting(false)
      }
   }

   return (
      <>
         <Card header={<HeadingLg>General blueprint fields</HeadingLg>} headerClassName="py-4">
            <BlueprintForm
               setFormik={setBlueprintFormik}
               initialValues={{ name: patternName ?? '', description: '' }}
            />
         </Card>
         {interpretations.map((interpretation, index) => (
            <InterpretationFormCard
               key={index}
               interpretation={interpretation}
               setFormik={(formik) => interpretationFormikSetter(formik, index)}
            />
         ))}
         <Button disabled={isSubmitting} onClick={handleSubmit}>
            Submit
         </Button>
      </>
   )
}
