import { useState } from 'react'

import { FormikProps } from 'formik'
import { isNil } from 'lodash'
import { useQuery, useQueryClient } from 'react-query'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'

import {
   AsylumApi,
   BlueprintChangeAdd,
   InterpretationCreate,
   InterpretationPattern,
} from '@asylum-ui/connection-library'

import { fetchBlueprints } from 'api'
import { Button } from 'components/button'
import { Heading } from 'components/text'
import { InterpretationFormValues } from 'modules/form/interpretation-form'
import { InterpretationCard } from 'modules/overview/overview-interpretation-list'
import { InterpretationFormCard } from 'pages/space-overview/blueprint-from-pattern/components/interpretation-form-card'
import {
   BlueprintUnwrappedForSelect,
   PatternInputSelect,
} from 'pages/space-overview/blueprint-from-pattern/extend-blueprint-from-pattern/pattern-input-select'
import { BlueprintUnwrapped, InterpretationUnwrapped } from 'types'
import { isMatchingInterpretation, touchAndValidate } from 'utils'

interface IInterpretations {
   resolved: InterpretationUnwrapped[]
   unresolved: InterpretationPattern[]
}

const splitInterpretations = (
   blueprint: BlueprintUnwrapped,
   interpretations: InterpretationPattern[]
) => {
   const splitInterpretations: IInterpretations = {
      resolved: [],
      unresolved: [],
   }
   interpretations.forEach((interpretationPattern) => {
      const resolvedInterpretation = blueprint.interpretations.find((interpretation) =>
         isMatchingInterpretation(interpretation, interpretationPattern)
      )

      if (resolvedInterpretation) {
         splitInterpretations.resolved.push(resolvedInterpretation)
      } else {
         splitInterpretations.unresolved.push(interpretationPattern)
      }
   })
   return splitInterpretations
}

interface ExtendBlueprintForPatternProps {
   interpretations: InterpretationPattern[]
}

export const ExtendBlueprintForPattern = ({ interpretations }: ExtendBlueprintForPatternProps) => {
   const queryClient = useQueryClient()
   const navigate = useNavigate()

   const { data: blueprints = [] } = useQuery('blueprints', fetchBlueprints)

   const [selectedBlueprint, setSelectedBlueprint] = useState<BlueprintUnwrapped | null>(null)

   const splittedInterpretations =
      selectedBlueprint && splitInterpretations(selectedBlueprint, interpretations)

   const [isSubmitting, setIsSubmitting] = useState(false)

   // eslint-disable-next-line func-call-spacing
   const [interpretationFormik, setInterpretationFormik] = useState<
      (FormikProps<InterpretationFormValues> | null)[]
   >([null])

   const interpretationFormikSetter = (
      formik: FormikProps<InterpretationFormValues>,
      index: number
   ) => {
      setInterpretationFormik((prevState) => {
         const newState = [...prevState]
         newState[index] = formik
         return newState
      })
   }

   const handleSubmit = async () => {
      if (interpretationFormik.some(isNil)) return

      interpretationFormik.forEach((formik) => touchAndValidate(formik as FormikProps<any>))
      if (interpretationFormik.some((formik) => !formik?.isValid)) return

      try {
         setIsSubmitting(true)
         const interpretations = (await Promise.all(
            interpretationFormik.map((formik) => formik!.submitForm())
         )) as (InterpretationCreate | undefined)[]

         if (interpretations.some(isNil) || !selectedBlueprint) return

         const changeSet = [new BlueprintChangeAdd(interpretations as InterpretationCreate[])]
         const blueprintId = parseInt(selectedBlueprint.id)

         await AsylumApi.updateBlueprint(blueprintId, changeSet)

         queryClient.invalidateQueries(['blueprints', selectedBlueprint.id])

         navigate('/overview')
      } catch (error) {
         toast.error(typeof error === 'string' ? error : 'Unknown Error')
      } finally {
         setIsSubmitting(false)
      }
   }

   const blueprintsForSelect: BlueprintUnwrappedForSelect[] = blueprints.map((blueprint) => {
      return {
         ...blueprint,
         unresolved: splitInterpretations(blueprint, interpretations).unresolved,
      }
   })

   return (
      <>
         <PatternInputSelect blueprints={blueprintsForSelect} onChange={setSelectedBlueprint} />
         {splittedInterpretations ? (
            <>
               {splittedInterpretations.resolved.map((interpretation, index) => (
                  <InterpretationCard key={index} interpretation={interpretation} />
               ))}
               {splittedInterpretations.unresolved.map((interpretation, index) => (
                  <InterpretationFormCard
                     key={index}
                     interpretation={interpretation}
                     setFormik={(formik) => interpretationFormikSetter(formik, index)}
                  />
               ))}
               {splittedInterpretations.unresolved.length ? (
                  <Button disabled={isSubmitting || !selectedBlueprint} onClick={handleSubmit}>
                     Submit
                  </Button>
               ) : (
                  <Heading className="m-auto">
                     Already Adapted. You can use this item in the space.
                  </Heading>
               )}
            </>
         ) : (
            <Heading className="m-auto">Select blueprint to adapt</Heading>
         )}
      </>
   )
}
