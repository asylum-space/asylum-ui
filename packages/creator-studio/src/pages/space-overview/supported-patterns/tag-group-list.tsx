import { InterpretationPattern } from '@asylum-ui/connection-library'

import { TagGroup } from 'components/tag'

interface TagGroupListProps {
   interpretations: InterpretationPattern[]
}

export const TagGroupList = ({ interpretations }: TagGroupListProps) => (
   <div className="flex flex-wrap gap-3">
      {interpretations
         .filter((interpretation) => !!interpretation.tags)
         .map((interpretation, index) => (
            <TagGroup key={index} tags={interpretation.tags!} />
         ))}
   </div>
)
