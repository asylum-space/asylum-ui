import { useState } from 'react'

import { observer } from 'mobx-react-lite'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'

import { fetchBlueprint } from 'api'
import { Button } from 'components/button'
import { Page } from 'components/page'
import { MintItemModal } from 'modules/modal'
import { Overview } from 'modules/overview'
import { useStore } from 'store'

export const BlueprintOverview = observer(() => {
   const store = useStore()
   const { id: blueprintId } = useParams()
   const [isMintModalOpen, setIsMintModalOpen] = useState(false)
   const MintItemButton = <Button onClick={() => setIsMintModalOpen(true)}>mint item</Button>

   if (blueprintId === undefined) return null

   const { data: blueprint } = useQuery(['blueprints', blueprintId], () =>
      fetchBlueprint(blueprintId)
   )

   if (!blueprint) return null

   const isOwner = store.account?.address === blueprint.issuer

   return (
      <>
         <Page
            title="Blueprint overview"
            returnButton={true}
            headerButton={isOwner && MintItemButton}
         >
            <Overview type="blueprint" data={blueprint} readonly={!isOwner} />
         </Page>
         <MintItemModal
            blueprintId={blueprint.id}
            open={isMintModalOpen}
            onClose={() => setIsMintModalOpen(false)}
         />
      </>
   )
})
