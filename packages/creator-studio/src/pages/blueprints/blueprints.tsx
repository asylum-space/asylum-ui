import { useEffect, useState } from 'react'

import { flow, map, sortBy, uniq } from 'lodash/fp'
import { observer } from 'mobx-react-lite'
import { useQuery } from 'react-query'
import { useLocation } from 'react-router-dom'

import { Pattern } from '@asylum-ui/connection-library'

import { fetchBlueprints, fetchSpaces } from 'api'
import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg'
import { Button, FilterButton } from 'components/button'
import { GridItems } from 'components/grid-items'
import { ListCardItem, ListCardItemSkeleton } from 'components/list-card-item'
import { Page } from 'components/page'
import { SearchAutocomplete } from 'components/search-autocomplete'
import { Paragraph } from 'components/text'
import { FilterModal, MintItemModal } from 'modules/modal'
import { BlueprintCreateModal } from 'modules/modal/blueprint-create-modal'
import { useStore } from 'store'
import { BlueprintUnwrapped, SpaceWithMetadataUnwrapped } from 'types'
import { filterMatchedSomePatterns } from 'utils'

export const Blueprints = observer(() => {
   const store = useStore()
   const { state: locationState } = useLocation() as any

   const [mintItemId, setMintItemId] = useState<string | null>(null)

   const [createModalOpen, setCreateModalOpen] = useState(false)

   const { data: blueprints = [], isLoading, isSuccess } = useQuery('blueprints', fetchBlueprints)

   const { data: spaces = [] } = useQuery('spaces', () => fetchSpaces(store.account!))

   const [selectedSpaces, setSelectedSpaces] = useState<SpaceWithMetadataUnwrapped[]>([])
   const [spacesFilterIsOpen, setSpacesFilterIsOpen] = useState(false)
   const [query, setQuery] = useState('')

   const filterBySpaces = (
      blueprints: BlueprintUnwrapped[],
      spaces: SpaceWithMetadataUnwrapped[]
   ): BlueprintUnwrapped[] => {
      const patterns = spaces
         .map((space) => space.patterns)
         .flat(2)
         .filter((pattern): pattern is Pattern => !!pattern)
      return filterMatchedSomePatterns(patterns, blueprints)
   }

   useEffect(() => {
      setSelectedSpaces(spaces.filter((space) => locationState?.spaceIds?.includes(space.id)))
   }, [spaces, locationState])

   const filterByQuery = (
      blueprints: BlueprintUnwrapped[],
      inputValue: string
   ): BlueprintUnwrapped[] =>
      blueprints.filter((item) => {
         return item.name?.toLowerCase().includes(inputValue.toLowerCase())
      })

   const filteredBlueprintsBySpaces = selectedSpaces.length
      ? filterBySpaces(blueprints, selectedSpaces)
      : blueprints

   const filteredBlueprintsByQuery = sortBy('id', filterByQuery(filteredBlueprintsBySpaces, query))

   return (
      <>
         <Page
            title="Blueprints"
            headerButton={
               <Button variant="light" onClick={() => setCreateModalOpen(true)}>
                  <PlusIcon className="fill-text-base w-4 h-4 inline-block mr-2" />
                  create blueprint
               </Button>
            }
         >
            <div className="flex gap-5 my-10">
               <SearchAutocomplete
                  className="grow"
                  variants={flow(map('name'), uniq)(blueprints)}
                  onVariantSelect={setQuery}
               />
               <FilterButton
                  filteredData={selectedSpaces}
                  onClick={() => setSpacesFilterIsOpen(true)}
               />
            </div>
            <GridItems dataTestId="blueprint-list">
               {isLoading && [...Array(6)].map((_, i) => <ListCardItemSkeleton key={i} />)}
               {isSuccess &&
                  filteredBlueprintsByQuery.map((item: BlueprintUnwrapped) => (
                     <ListCardItem
                        key={item.id}
                        id={item.id}
                        title={item.name}
                        description={item.metadata.description}
                        interpretation={item.defaultInterpretation}
                        address={item.issuer}
                        to={`${item.id}`}
                        actionButton={
                           item.issuer === store.account?.address
                              ? {
                                   text: 'mint item',
                                   onClick: () => setMintItemId(item.id),
                                }
                              : undefined
                        }
                     />
                  ))}
            </GridItems>
            {query && filteredBlueprintsByQuery.length === 0 && (
               <Paragraph className="text-white">
                  Your search - <strong>{query}</strong> - did not match any Blueprints.
               </Paragraph>
            )}
            {isSuccess && blueprints.length === 0 && (
               <Paragraph className="text-white">No Blueprints created.</Paragraph>
            )}
         </Page>
         <MintItemModal
            blueprintId={mintItemId}
            open={mintItemId !== null}
            onClose={() => setMintItemId(null)}
         />
         <BlueprintCreateModal open={createModalOpen} onClose={() => setCreateModalOpen(false)} />
         <FilterModal
            title="Filter Blueprints"
            values={sortBy('id', spaces)}
            valuesName="spaces"
            initialValue={selectedSpaces}
            optionSelector={(spaces: SpaceWithMetadataUnwrapped) => spaces.title}
            onApply={setSelectedSpaces}
            open={spacesFilterIsOpen}
            onClose={() => setSpacesFilterIsOpen(false)}
         />
      </>
   )
})
