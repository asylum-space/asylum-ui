import { useState } from 'react'

import classNames from 'classnames'
import { FormikProps } from 'formik'
import { useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'

import {
   AsylumApi,
   BlueprintChangeModify,
   InterpretationCreate,
} from '@asylum-ui/connection-library'

import { fetchTags } from 'api'
import { ReactComponent as EditIcon } from 'assets/svg/pen.svg'
import { Button } from 'components/button'
import { Modal } from 'components/modal'
import { InterpretationForm, InterpretationFormValues } from 'modules/form/interpretation-form'
import { InterpretationUnwrapped } from 'types'
import { findEditableFields, touchAndValidate } from 'utils'

interface IEditInterpretationModal {
   blueprintId: number
   interpretation?: InterpretationUnwrapped
   open: boolean
   onClose: () => void
}

export const EditInterpretationModal = ({
   blueprintId,
   interpretation,
   open,
   onClose,
}: IEditInterpretationModal) => {
   const queryClient = useQueryClient()
   const { data: allTags = [] } = useQuery('tags', fetchTags)

   const [interpretationFormik, setInterpretationFormik] =
      useState<FormikProps<InterpretationFormValues> | null>(null)

   const [isSubmitting, setIsSubmitting] = useState(false)

   const handleSubmit = async () => {
      if (!interpretationFormik || !interpretation) return
      await touchAndValidate(interpretationFormik)
      if (!interpretationFormik.isValid) return
      try {
         setIsSubmitting(true)
         const newInterpretation = (await interpretationFormik.submitForm()) as
            | InterpretationCreate
            | undefined
         if (!newInterpretation) return

         const changeSet = [
            new BlueprintChangeModify([[interpretation.id.split(':')[1], newInterpretation]]),
         ]

         await AsylumApi.updateBlueprint(blueprintId, changeSet)
         toast.success('Interpretation edited!')
         queryClient.invalidateQueries(['blueprints', blueprintId])
         onClose && onClose()
      } catch (error) {
         toast.error(typeof error === 'string' ? error : 'Unknown Error')
      } finally {
         setIsSubmitting(false)
      }
   }

   const initialTags = allTags.filter((tag) => interpretation?.tags.includes(tag.id))

   const initialValues: InterpretationFormValues = {
      tags: initialTags,
      src: interpretation?.interpretationInfo.src,
      editableTagFields: findEditableFields(initialTags).reduce((prev, field) => {
         return {
            ...prev,
            [field.name]: interpretation?.interpretationInfo.metadata[field.name] ?? null,
         }
      }, {}),
   }

   return (
      <Modal open={open} onClose={onClose} title="Edit interpretation" maxWidth="2xl">
         <div className="flex flex-col gap-7 p-4">
            <InterpretationForm setFormik={setInterpretationFormik} initialValues={initialValues} />
            <Button
               variant="light"
               className={classNames({ 'animate-pulse': isSubmitting })}
               onClick={handleSubmit}
               disabled={isSubmitting}
            >
               {isSubmitting ? (
                  'Submitting'
               ) : (
                  <>
                     <EditIcon className="inline-block mr-2" /> edit interpretation
                  </>
               )}
            </Button>
         </div>
      </Modal>
   )
}
