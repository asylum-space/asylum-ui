import { useContext } from 'react'

import { useFormik } from 'formik'
import { observer } from 'mobx-react-lite'
import { useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'

import { fetchBlueprint, mintItem, uploadMetadata } from 'api'
import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg'
import { Button } from 'components/button'
import { InputField, InputFieldSkeleton, InputLabel } from 'components/input'
import { JsonRaw } from 'components/json-raw'
import { Modal } from 'components/modal'
import { ColorContext } from 'context'
import { useStore } from 'store'
import { ComponentProps } from 'types'
import { formatAddress } from 'utils'

interface MintItemModalProps {
   blueprintId: string | null
   onClose: () => void
   open: boolean
}

interface IMintItemFormValues {
   name: string
   owner: string
}

export const MintItemModal = observer(
   ({ open, blueprintId, onClose }: ComponentProps<MintItemModalProps>) => {
      const store = useStore()
      const queryClient = useQueryClient()
      const colorContextValue = useContext(ColorContext)

      const { data: blueprint, isSuccess: blueprintIsSuccess } = useQuery(
         ['blueprint', blueprintId],
         async () => fetchBlueprint(blueprintId as string),
         {
            enabled: blueprintId !== null,
         }
      )

      const formik = useFormik<IMintItemFormValues>({
         initialValues: {
            name: blueprint?.name ? `${blueprint.name} NFT` : '',
            owner: store.account?.address || '',
         },
         validate: (values) => {
            const errors: any = {}

            if (values.name === '') {
               errors.name = 'Name is required'
            }

            if (values.owner === '') {
               // TODO: Add proper validation with Polkadot package?
               // https://docs.substrate.io/v3/advanced/ss58/
               errors.owner = 'Owner is required'
            }

            return errors
         },
         onSubmit: async (values) => {
            try {
               if (!blueprint) {
                  throw new Error('no blueprint')
               }
               const CID = await uploadMetadata(extendedMetaData)
               await mintItem(values.owner, blueprint.id, CID)
               toast.success(`Item ${values.name} was created`)
               queryClient.invalidateQueries('items')
               onClose && onClose()
            } catch (error) {
               if (error instanceof Error) {
                  toast.error(error.message)
               }
            } finally {
               formik.setSubmitting(false)
            }
         },
         enableReinitialize: true,
      })

      const handleClose = () => {
         formik.resetForm()
         onClose && onClose()
      }

      const extendedMetaData = {
         name: formik.values.name,
         description: blueprint?.metadata.description || '',
      }

      return (
         <Modal maxWidth="2xl" title="Mint Item" open={open} onClose={handleClose}>
            <form className="p-4 flex flex-col gap-4 pb-8" onSubmit={formik.handleSubmit}>
               <div className="flex gap-4">
                  {blueprint ? (
                     <InputField
                        as="input"
                        className="basis-8/12"
                        label="Name"
                        placeholder="Name"
                        name="name"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        errorMessage={formik.touched.name && formik.errors.name}
                     />
                  ) : (
                     <InputFieldSkeleton className="basis-8/12" label="Name" />
                  )}
                  {blueprint ? (
                     <InputField
                        as="input"
                        className="basis-4/12"
                        value={formatAddress(blueprint.issuer)}
                        label="Issuer"
                        placeholder="Issuer Address"
                        name="issuer"
                        readOnly
                        disabled
                     />
                  ) : (
                     <InputFieldSkeleton label="Issuer" className="basis-4/12" />
                  )}
               </div>
               <InputField
                  as="input"
                  label="Owner"
                  placeholder="owner"
                  name="owner"
                  value={formik.values.owner}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  errorMessage={formik.touched.owner && formik.errors.owner}
               />
               <div>
                  <InputLabel className="mb-2">Item Metadata</InputLabel>
                  {blueprint ? (
                     <JsonRaw metadata={extendedMetaData} />
                  ) : (
                     <InputFieldSkeleton inputClassName="h-12" />
                  )}
               </div>
               <div>
                  <InputLabel className="mb-2">Interpretations Metadata</InputLabel>
                  {blueprint ? (
                     <JsonRaw
                        className="overflow-y-scroll max-h-80"
                        metadata={blueprint.interpretations}
                     />
                  ) : (
                     <InputFieldSkeleton inputClassName="h-48" />
                  )}
               </div>
               <Button
                  variant="light"
                  className="mt-7"
                  surroundColor={colorContextValue}
                  disabled={formik.isSubmitting || !blueprintIsSuccess}
               >
                  <PlusIcon className="fill-text-base w-4 h-4 inline-block mr-2" /> Mint item
               </Button>
            </form>
         </Modal>
      )
   }
)
