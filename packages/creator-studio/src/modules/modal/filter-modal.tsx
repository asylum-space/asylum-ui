import { useContext, useEffect, useState } from 'react'

import { Button } from 'components/button'
import { InputSelect } from 'components/input-select'
import { Modal, ModalProps } from 'components/modal'
import { ColorContext } from 'context'

interface IFilterItemsModal extends ModalProps {
   values: any[]
   valuesName: string
   onApply: any
   initialValue: any[]
   optionSelector: (option: any) => string
}

export const FilterModal = ({
   title = 'Filter',
   values,
   valuesName,
   onApply,
   initialValue,
   optionSelector,
   onClose,
   ...rest
}: IFilterItemsModal) => {
   const colorContextValue = useContext(ColorContext)

   const [value, setValue] = useState(initialValue)

   useEffect(() => {
      setValue(initialValue)
   }, [initialValue])

   const handleReset = () => {
      onApply([])
      setValue([])
      onClose && onClose()
   }
   const handleApply = () => {
      onApply(value)
      onClose && onClose()
   }

   return (
      <Modal onClose={onClose} title={title} {...rest}>
         <div className="p-4 flex flex-col gap-6 pb-8">
            <InputSelect<any[], true>
               surroundColor={colorContextValue}
               isMulti={true}
               placeholder={`Select ${valuesName}`}
               name="blueprints"
               value={value}
               options={values}
               onChange={(value) => setValue(value as any[])}
               getOptionLabel={optionSelector}
               getOptionValue={optionSelector}
            />
            <div className="flex gap-6">
               <Button onClick={handleApply} className="flex-1">
                  apply
               </Button>
               <Button onClick={handleReset} className="flex-1">
                  reset
               </Button>
            </div>
         </div>
      </Modal>
   )
}
