import { useState } from 'react'

import classNames from 'classnames'
import { FormikProps } from 'formik'
import 'highlight.js/styles/github.css'
import { observer } from 'mobx-react-lite'
import { useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'

import { AsylumApi, BlueprintCreate, InterpretationCreate } from '@asylum-ui/connection-library'

import { fetchTags } from 'api'
import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg'
import { Button } from 'components/button'
import { Modal } from 'components/modal'
import { Tag } from 'components/tag'
import { Paragraph } from 'components/text'
import { BlueprintForm, BlueprintFormValues } from 'modules/form/blueprint-form'
import { InterpretationForm, InterpretationFormValues } from 'modules/form/interpretation-form'
import { findDefaultTag, touchAndValidate } from 'utils'

interface IBlueprintCreateModal {
   open: boolean
   onClose?: () => void
}

export const BlueprintCreateModal = observer(({ open, onClose }: IBlueprintCreateModal) => {
   const queryClient = useQueryClient()
   const { data: tags } = useQuery('tags', fetchTags)

   const [blueprintFormik, setBlueprintFormik] = useState<FormikProps<BlueprintFormValues> | null>(
      null
   )
   const [interpretationFormik, setInterpretationFormik] =
      useState<FormikProps<InterpretationFormValues> | null>(null)

   const [isSubmitting, setIsSubmitting] = useState(false)

   const isAnythingSubmitting =
      isSubmitting || blueprintFormik?.isSubmitting || interpretationFormik?.isSubmitting

   const formikArray = [blueprintFormik, interpretationFormik]

   const submitHandler = async () => {
      if (!blueprintFormik || !interpretationFormik) return
      formikArray.forEach((formik) => touchAndValidate(formik as FormikProps<any>))
      if (formikArray.some((formik) => !formik?.isValid)) return
      try {
         setIsSubmitting(true)
         const blueprint = (await blueprintFormik.submitForm()) as BlueprintCreate | undefined
         const interpretation = (await interpretationFormik.submitForm()) as
            | InterpretationCreate
            | undefined
         if (!blueprint || !interpretation) return
         await AsylumApi.createBlueprint(blueprint.name, blueprint.metadata, 100, [interpretation])
         toast.success(`Blueprint "${blueprint.name}" created!`)
         // TODO: use some callback or any other way to wait until blueprint is on chain
         setTimeout(() => queryClient.invalidateQueries('blueprints'), 100)
         onClose && onClose()
      } catch (error) {
         toast.error(typeof error === 'string' ? error : 'Unknown Error')
      } finally {
         setIsSubmitting(false)
      }
   }

   const defaultTag = tags && (findDefaultTag(tags) || null)

   const interpretationInitialValues = {
      tags: defaultTag ? [defaultTag] : [],
      src: undefined,
      editableTagFields: {},
   }

   return (
      <Modal
         open={open}
         onClose={onClose}
         title="Create Blueprint"
         className="text-white"
         maxWidth="2xl"
      >
         <div className="p-4 flex flex-col gap-4 pb-8">
            <BlueprintForm setFormik={setBlueprintFormik} />
            <Paragraph className="text-white mt-2 mx-1">
               You have to create at least one interpretation with tag{' '}
               <Tag className="mx-px">default-view</Tag>, which will be used for marketplace
               representation or inventory.
            </Paragraph>
            {defaultTag !== undefined && (
               <InterpretationForm
                  setFormik={setInterpretationFormik}
                  requiresDefault={true}
                  initialValues={interpretationInitialValues}
               />
            )}
            <Button
               variant="light"
               onClick={submitHandler}
               disabled={isAnythingSubmitting}
               surroundColor="gray"
               className={classNames('mt-4', { 'animate-pulse': isAnythingSubmitting })}
            >
               {isAnythingSubmitting ? (
                  'Submitting'
               ) : (
                  <>
                     <PlusIcon className="fill-text-base w-4 h-4 inline-block mr-2" /> create
                     blueprint
                  </>
               )}
            </Button>
         </div>
      </Modal>
   )
})
