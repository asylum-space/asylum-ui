import { useState } from 'react'

import classNames from 'classnames'
import { observer } from 'mobx-react-lite'

import { ButtonWithIcon } from 'components/button'
import { useStore } from 'store'

import { NodeConnectModal } from './node-connect-modal'

export const NodeConnect = observer(() => {
   const store = useStore()
   const buttonText = store.network?.name || 'disconnected'
   const [open, setOpen] = useState(false)

   const Icon = (
      <span
         className={classNames('h-1.5 w-1.5 rounded-full', {
            'bg-green-600': store.isConnected,
            'bg-red-600': !store.isConnected,
         })}
      />
   )

   return (
      <>
         <ButtonWithIcon
            icon={Icon}
            onClick={() => setOpen(true)}
            className="whitespace-nowrap overflow-hidden"
         >
            {buttonText}
         </ButtonWithIcon>
         <NodeConnectModal open={open} onClose={() => setOpen(false)} />
      </>
   )
})
