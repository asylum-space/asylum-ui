import { useContext, useEffect } from 'react'

import { FormikErrors, FormikProps, useFormik } from 'formik'
import { isNil } from 'lodash'
import { map } from 'lodash/fp'
import { useQuery } from 'react-query'
import { OptionProps, components } from 'react-select'

import {
   AsylumApi,
   CID,
   InterpretationCreate,
   InterpretationPattern,
   TagMetadata,
} from '@asylum-ui/connection-library'

import { fetchTags } from 'api'
import { InputField, InputFieldSkeleton, InputFileUpload, InputLabel } from 'components/input'
import { InputSelect } from 'components/input-select'
import { JsonRaw } from 'components/json-raw'
import { TagGroup } from 'components/tag'
import { ColorContext } from 'context'
import { findDefaultTag, findEditableFields, generateTagMetadata } from 'utils'

interface editableTagFields {
   [key: string]: string
}

export interface InterpretationFormValues {
   tags: TagMetadata[]
   src?: CID
   editableTagFields: editableTagFields
}

interface InterpretationFormProps {
   setFormik: (formik: FormikProps<InterpretationFormValues>) => void
   initialValues?: InterpretationFormValues
   requiresDefault?: boolean
   lockedValues?: InterpretationPattern
}

const defaultValues: InterpretationFormValues = {
   tags: [],
   src: undefined,
   editableTagFields: {},
}

export const InterpretationForm = ({
   initialValues,
   requiresDefault = false,
   setFormik,
   lockedValues,
}: InterpretationFormProps) => {
   const colorContextValue = useContext(ColorContext)

   const { data: tags } = useQuery('tags', fetchTags)

   const formik = useFormik<InterpretationFormValues>({
      initialValues: initialValues || defaultValues,
      validate: (values) => {
         const errors: FormikErrors<InterpretationFormValues> = {}

         if (!values.tags.length) {
            errors.tags = 'At least one tag is required'
         }

         if (values.tags.length) {
            const { conflictedFields, conflictedTags } = generateTagMetadata(
               values.tags,
               values.editableTagFields
            )

            if (conflictedFields.length) {
               errors.tags = `Tags ${JSON.stringify(
                  map('id', conflictedTags)
               )} have conflicting metadata fields: ${JSON.stringify(conflictedFields)}`
            }

            if (requiresDefault && !findDefaultTag(values.tags)) {
               errors.tags = 'Default Tag is Required'
            }

            for (const field of findEditableFields(values.tags)) {
               if (
                  isNil(values.editableTagFields[field.name]) ||
                  values.editableTagFields[field.name] === ''
               ) {
                  if (!errors.editableTagFields) errors.editableTagFields = {}
                  errors.editableTagFields[field.name] = `${field.name} is required`
               }
            }
         }

         if (!values.src) {
            errors.src = 'Interpretation source is required'
         }
         return errors
      },
      onSubmit: async (values, { setSubmitting }) => {
         try {
            const MetadataCID = await AsylumApi.uploadMetadata(
               generateTagMetadata(values.tags, values.editableTagFields).metadata
            )
            return {
               tags: values.tags.map((tag) => tag.id),
               interpretation: {
                  src: values.src,
                  metadata: MetadataCID,
               },
            } as InterpretationCreate
         } catch (error) {
            console.error(error)
         } finally {
            setSubmitting(false)
         }
      },
      validateOnMount: true,
      enableReinitialize: true,
   })

   const handleSourceLoad = async (buffer: ArrayBuffer) => {
      const CID = await AsylumApi.uploadFile(buffer)
      await formik.setFieldValue('src', CID)
   }

   const handleTagsChange = (newTags: TagMetadata[]) => {
      const editableFields = findEditableFields(newTags)
      const newEditableFields = editableFields.reduce((prev, field) => {
         return {
            ...prev,
            [field.name]: formik.values.editableTagFields[field.name] ?? field.defaultValue ?? '',
         }
      }, {})
      formik.setValues({
         ...formik.values,
         tags: newTags,
         editableTagFields: newEditableFields,
      })

      const touchedFields = formik.touched.editableTagFields
      if (touchedFields) {
         formik.setTouched({
            ...formik.touched,
            editableTagFields: editableFields.reduce((prev, field) => {
               return {
                  ...prev,
                  [field.name]: touchedFields[field.name],
               }
            }, {}),
         })
      }
   }

   useEffect(() => {
      setFormik && setFormik(formik)
   }, [formik.values, formik.values.tags.length, formik.isSubmitting, formik.isValid])

   return (
      <form className="text-white flex flex-col gap-4">
         {lockedValues?.tags ? (
            <TagGroup tags={formik.values.tags.map((tag) => tag.id)} className="py-1" />
         ) : tags ? (
            <InputSelect<TagMetadata, true>
               isMulti={true}
               surroundColor={colorContextValue}
               placeholder="Select tags"
               name="tags"
               value={formik.values.tags}
               onChange={(value) => handleTagsChange(value as TagMetadata[])}
               onBlur={() => formik.setTouched({ ...formik.touched, tags: true as any })}
               options={tags}
               getOptionLabel={(option) => option.id}
               getOptionValue={(option) => option.id}
               errorMessage={formik.touched.tags && (formik.errors.tags as string)}
               components={{
                  Option,
               }}
               dataTestId="interpretation-select"
            />
         ) : (
            <InputFieldSkeleton />
         )}
         <InputFileUpload
            accept="*"
            name="source"
            label="Source"
            value={formik.values.src}
            errorMessage={formik.touched.src ? formik.errors.src : undefined}
            onLoad={handleSourceLoad}
            dataTestId="interpretation-file-upload"
         />
         {formik.values.tags.map((tag) => {
            const editableFields = findEditableFields(tag)

            if (editableFields.length === 0) return null
            return (
               <div key={tag.id}>
                  <InputLabel>{tag.id}</InputLabel>
                  <div className="grid grid-cols-2 gap-2 gap-x-6">
                     {editableFields.map((field) => (
                        <InputField
                           as="input"
                           type={field.type}
                           labelTooltipMessage={field.description}
                           key={field.name}
                           name={`editableTagFields.${field.name}`}
                           label={field.name}
                           errorMessage={
                              formik.touched.editableTagFields?.[field.name] &&
                              formik.errors.editableTagFields?.[field.name]
                           }
                           value={
                              formik.values.editableTagFields[field.name] ??
                              field.defaultValue ??
                              ''
                           }
                           onChange={formik.handleChange}
                           onBlur={formik.handleBlur}
                           placeholder={field.type === 'number' ? '0' : field.description}
                           disabled={
                              lockedValues?.fields?.find(
                                 (patternField: { name: string; value?: string | number }) =>
                                    patternField.name === field.name
                              )?.value !== undefined
                           }
                        />
                     ))}
                  </div>
               </div>
            )
         })}

         <div>
            <InputLabel className="mb-2">Raw Metadata</InputLabel>
            <JsonRaw
               metadata={{
                  src: formik.values.src,
                  metadata: generateTagMetadata(formik.values.tags, formik.values.editableTagFields)
                     .metadata,
               }}
            />
         </div>
      </form>
   )
}

const Option = (props: OptionProps<TagMetadata, true>) => {
   return (
      <components.Option {...props}>
         {props.label} <span className="text-gray-400">- {props.data.description}</span>
      </components.Option>
   )
}
