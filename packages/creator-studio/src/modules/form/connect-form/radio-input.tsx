import { ChangeEvent, ComponentPropsWithoutRef, useRef } from 'react'

import classNames from 'classnames'
import { FieldInputProps } from 'formik'

import { InputField } from 'components/input'

interface RadioInputProps extends ComponentPropsWithoutRef<'input'> {
   label?: string
   custom?: boolean
   customGetFieldProps?: FieldInputProps<any>
}

export const RadioInput = ({
   label,
   value,
   custom = false,
   name,
   customGetFieldProps,
   onChange,
   ...rest
}: RadioInputProps) => {
   const radioRef = useRef<HTMLInputElement>(null)

   const handleFocus = () => {
      if (!radioRef.current) return
      radioRef.current.checked = true
      onChange &&
         onChange({ target: { name, value: 'custom' } } as ChangeEvent<HTMLInputElement>)
   }

   return (
      <label className="flex items-center gap-2 ml-3 cursor-pointer group">
         <input
            ref={radioRef}
            type="radio"
            name={name}
            value={value}
            onChange={onChange}
            {...rest}
         />
         {custom ? (
            <InputField
               as="input"
               {...customGetFieldProps}
               onFocus={handleFocus}
               placeholder={name === 'node' ? 'wss://node.custom:9944' : 'https://ipfs.custom:5001'}
               inputClassName={classNames(
                  'transition-colors',
                  radioRef.current?.checked
                     ? 'bg-neutral-700'
                     : 'bg-transparent group-hover:bg-neutral-800'
               )}
            />
         ) : (
            <div
               className={classNames(
                  'text-base font-primary grow px-3 py-2.5 rounded flex justify-between transition-colors',
                  radioRef.current?.checked
                     ? 'bg-neutral-700'
                     : 'bg-transparent group-hover:bg-neutral-800'
               )}
            >
               <span>{label}</span>
               <span>{value}</span>
            </div>
         )}
      </label>
   )
}
