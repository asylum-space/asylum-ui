import { useContext, useState } from 'react'

import classNames from 'classnames'
import { useFormik } from 'formik'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'
import * as yup from 'yup'

import { AsylumApi } from '@asylum-ui/connection-library'

import { Button } from 'components/button'
import { Heading, Paragraph } from 'components/text'
import { ColorContext } from 'context'
import { useStore } from 'store'
import { INetwork } from 'types'

import { RadioInput } from './radio-input'

interface ConnectFormik {
   node: string
   ipfs: string
   customNode: string
   customIpfs: string
}

interface ConnectFormProps {
   onClose?: () => void
}

interface Options {
   [name: string]: string
}

const NODE_OPTIONS: Options = {
   local: 'ws://127.0.0.1:9944',
   testnet: 'wss://193.169.1.52:9944',
} as const

const IPFS_OPTIONS: Options = {
   local: 'http://127.0.0.1:5001',
   testnet: 'https://193.169.1.52:5001',
} as const

export const ConnectForm = observer(({ onClose }: ConnectFormProps) => {
   const colorContextValue = useContext(ColorContext)

   const store = useStore()
   const navigate = useNavigate()
   const [error, setError] = useState<string | null>(null)

   const buildInitialValues = (): ConnectFormik => {
      let node = NODE_OPTIONS.local
      let ipfs = IPFS_OPTIONS.local
      const customNode = ''
      let customIpfs = ''

      if (store.network?.nodeUrl) {
         if (
            Object.values(NODE_OPTIONS).some((option) => store.network!.nodeUrl.startsWith(option))
         ) {
            node = store.network.nodeUrl
         } else {
            node = 'custom'
            customIpfs = store.network.nodeUrl
         }
      }

      if (store.network?.ipfsUrl) {
         if (
            Object.values(IPFS_OPTIONS).some((option) => store.network!.ipfsUrl.startsWith(option))
         ) {
            ipfs = store.network.ipfsUrl
         } else {
            ipfs = 'custom'
            customIpfs = store.network.ipfsUrl
         }
      }

      return {
         node,
         ipfs,
         customNode,
         customIpfs,
      }
   }

   const formik = useFormik<ConnectFormik>({
      initialValues: buildInitialValues(),
      validationSchema: yup.object({
         node: yup.string().required(),
         ipfs: yup.string().required(),
      }),
      onSubmit: async (values, formikHelpers) => {
         const name = `${
            Object.keys(NODE_OPTIONS).find((key) => NODE_OPTIONS[key] === values.node) ?? 'custom'
         } node`

         const network: INetwork = {
            name,
            nodeUrl: values.node === 'custom' ? values.customNode : values.node,
            ipfsUrl: values.ipfs === 'custom' ? values.customIpfs : values.ipfs,
         }

         try {
            await AsylumApi.connect(network, () => onConnected(network), onDisconnected)
         } catch (e) {
            setTimeout(
               () =>
                  setError('Cannot connect to the endpoint: ' + network.nodeUrl + '. Try again.'),
               200
            )
         } finally {
            formikHelpers.setSubmitting(false)
         }
      },
   })

   const onConnected = (network: INetwork) => {
      setError(null)
      store.setNetwork(network)
      store.setIsConnected(true)
      onClose && onClose()
   }
   const onDisconnected = () => {
      store.clear()
      navigate('/')
   }

   const handleDisconnect = async () => {
      onClose && onClose()
      formik.resetForm()
      await AsylumApi.disconnect()
      onDisconnected()
   }

   return (
      <form onSubmit={formik.handleSubmit} className="p-4 flex flex-col gap-12">
         {(
            [
               { options: NODE_OPTIONS, field: 'node' },
               { options: IPFS_OPTIONS, field: 'ipfs' },
            ] as { options: Options; field: keyof ConnectFormik }[]
         ).map(({ options, field }) => (
            <div key={field} className="flex flex-col gap-3">
               <Heading>
                  <span className={classNames(field === 'ipfs' ? 'uppercase' : 'capitalize')}>
                     {field}
                  </span>{' '}
                  endpoint:
               </Heading>
               {Object.entries(options).map(([label, url]) => (
                  <RadioInput
                     key={label}
                     label={label}
                     {...formik.getFieldProps(field)}
                     value={url}
                     checked={formik.values[field].startsWith(url)}
                  />
               ))}
               <RadioInput
                  key={`${field} custom`}
                  custom={true}
                  customGetFieldProps={formik.getFieldProps(
                     (field === 'node' ? 'customNode' : 'customIpfs') as keyof ConnectFormik
                  )}
                  {...formik.getFieldProps(field)}
                  value="custom"
                  checked={formik.values[field] === 'custom'}
               />
               {formik.touched[field] && formik.errors[field] && (
                  <Paragraph className="text-red-300 text-center">{formik.errors[field]}</Paragraph>
               )}
            </div>
         ))}
         {error && <Paragraph className="text-center text-red-400">{error}</Paragraph>}
         <div className="flex gap-4">
            <Button
               variant="dark"
               className="flex-1 bg-neutral-800"
               type="button"
               surroundColor={colorContextValue}
               onClick={handleDisconnect}
            >
               disconnect
            </Button>
            <Button
               className={classNames('flex-1', formik.isSubmitting && 'animate-pulse')}
               type="submit"
               surroundColor={colorContextValue}
               disabled={formik.isSubmitting || !!(!formik.isValid && formik.submitCount)}
            >
               {formik.isSubmitting ? 'connecting...' : 'connect'}
            </Button>
         </div>
      </form>
   )
})
