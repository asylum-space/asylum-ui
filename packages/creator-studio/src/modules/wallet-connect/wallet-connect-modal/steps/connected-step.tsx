import { observer } from 'mobx-react-lite'

import { AddressName } from '../components/address-name'
import { CopyAddressButton } from '../components/copy-address-button'
import { Button } from 'components/button'
import { useStore } from 'store'
import { ComponentProps } from 'types'

import { WalletConnectStepType, WalletStepProps } from './steps'

export const ConnectedStep = observer(({ nextStep, onClose }: ComponentProps<WalletStepProps>) => {
   const store = useStore()

   const handleChange = () => {
      nextStep(WalletConnectStepType.AccountSelection)
   }

   const handleDisconnect = () => {
      localStorage.clear()
      store.setAccount(null)
      onClose()
   }

   return (
      <>
         {store.account && (
            <div className="flex justify-between gap-4 mb-4">
               <AddressName name={store.account.meta.name} />
               <CopyAddressButton address={store.account.address} />
            </div>
         )}
         <div className="flex gap-6 mt-6">
            <Button variant="dark" onClick={handleDisconnect} className="flex-1 bg-neutral-800">
               disconnect
            </Button>
            <Button onClick={handleChange} className="flex-1">
               change wallet
            </Button>
         </div>
      </>
   )
})
