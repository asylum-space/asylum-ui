import { useEffect, useState } from 'react'

import { web3Accounts, web3FromAddress } from '@polkadot/extension-dapp'
import { observer } from 'mobx-react-lite'
import { useQueryClient } from 'react-query'

import { AsylumApi } from '@asylum-ui/connection-library'

import { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import { Avatar } from 'components/avatar'
import { Button, ButtonWithIcon } from 'components/button'
import { Paragraph } from 'components/text'
import { useStore } from 'store'
import { ComponentProps } from 'types'
import { formatAddress } from 'utils'

import { WalletConnectStepType, WalletStepProps } from './steps'

interface AccountRowProps {
   account: InjectedAccountWithMeta
   onSelect: (account: InjectedAccountWithMeta) => void
}

const AccountRow = ({ account, onSelect }: ComponentProps<AccountRowProps>) => (
   <ButtonWithIcon
      icon={<Avatar />}
      variant="dark"
      key={account.address}
      className="w-full text-left flex items-center justify-between "
      onClick={() => onSelect(account)}
   >
      <span className="flex text-left items-center gap-4">{account.meta.name}</span>
      {formatAddress(account.address)}
   </ButtonWithIcon>
)

const AccountsEmpty = () => (
   <Paragraph className="text-center">
      No accounts found. Please setup accounts and click refresh.
   </Paragraph>
)

export const AccountSelectionStep = observer(
   ({ nextStep, onClose }: ComponentProps<WalletStepProps>) => {
      const queryClient = useQueryClient()
      const store = useStore()
      const [accounts, setAccounts] = useState<InjectedAccountWithMeta[]>([])

      const handleConnect = async (account: InjectedAccountWithMeta) => {
         store.setSelectedSpace(null)
         store.setAccount(account)
         queryClient.invalidateQueries()
         const injector = await web3FromAddress(account.address)
         AsylumApi.withInjectedSigner(account.address, injector.signer)
         setTimeout(() => nextStep(WalletConnectStepType.Connected), 150)
         onClose()
      }

      const fetchAccounts = async () => {
         const allAccounts = await web3Accounts()
         setAccounts(allAccounts)
      }

      useEffect(() => {
         fetchAccounts().catch(console.error)
      }, [])

      if (accounts.length === 0) {
         return (
            <>
               <AccountsEmpty />
               <Button className="w-full mt-8" onClick={fetchAccounts}>
                  refresh
               </Button>
            </>
         )
      }

      if (accounts.length === 1) {
         handleConnect(accounts[0])
      }

      return (
         <div className="flex flex-col gap-4">
            {accounts.map((account) => (
               <AccountRow key={account.address} account={account} onSelect={handleConnect} />
            ))}
         </div>
      )
   }
)
