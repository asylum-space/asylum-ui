import { useState } from 'react'

import { web3Enable } from '@polkadot/extension-dapp'

import { ReactComponent as ArrowRightIcon } from 'assets/svg/arrow-right.svg'
import { ReactComponent as PolkadotIcon } from 'assets/svg/polkadot.svg'
import { Button } from 'components/button'
import { Paragraph } from 'components/text'
import { ComponentProps } from 'types'

import { WalletConnectStepType, WalletStepProps } from './steps'

export const ExtensionSelectionStep = ({ nextStep }: ComponentProps<WalletStepProps>) => {
   const [error, setError] = useState(false)

   const handleConnect = async () => {
      setError(false)
      const extensions = await web3Enable('Asylum')
      if (extensions.length !== 0) {
         nextStep(WalletConnectStepType.AccountSelection)
      } else {
         setError(true)
      }
   }

   return (
      <>
         <Button
            variant="light"
            className="w-full text-left flex items-center justify-between"
            onClick={handleConnect}
            error={error}
         >
            <span className="flex text-left items-center gap-4">
               <PolkadotIcon /> {'Polkadot{.js}'}
            </span>
            <ArrowRightIcon className="justify-self-end" />
         </Button>
         {error && (
            <Paragraph className="text-center text-red-400 mt-7">
               Failed to connect with PolkadotJS extension. Please try again.
            </Paragraph>
         )}
      </>
   )
}
