import * as React from 'react'

import { AccountSelectionStep } from './account-selection-step'
import { ConnectedStep } from './connected-step'
import { ExtensionSelectionStep } from './extension-selection-step'

export enum WalletConnectStepType {
   ExtensionSelection,
   AccountSelection,
   Connected,
}

export interface WalletStepProps {
   nextStep: (step: WalletConnectStepType) => void
   onClose: () => void
}

type WalletConnectStep = Record<
   WalletConnectStepType,
   { component: (arg: WalletStepProps) => React.ReactNode; name: string }
>

export const WALLET_CONNECT_STEPS: WalletConnectStep = {
   [WalletConnectStepType.ExtensionSelection]: {
      component: (props: WalletStepProps) => <ExtensionSelectionStep {...props} />,
      name: 'Select Extension',
   },
   [WalletConnectStepType.AccountSelection]: {
      component: (props: WalletStepProps) => <AccountSelectionStep {...props} />,
      name: 'Select Wallet',
   },
   [WalletConnectStepType.Connected]: {
      component: (props: WalletStepProps) => <ConnectedStep {...props} />,
      name: 'Connected Wallet',
   },
}
