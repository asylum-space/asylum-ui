import { createContext, useState } from 'react'

import { AddInterpretationModal } from 'modules/modal'
import { BlueprintUnwrapped, ComponentProps, ItemUnwrapped } from 'types'

import { OverviewCard } from './overview-card'
import { InterpretationList } from './overview-interpretation-list'
import { ScreenshotsFromSpace } from './screenshots-from-space'

interface OverviewProps {
   onAcceptItemUpdateClick?: () => void
}

type ItemProps = {
   type: 'item'
   data: ItemUnwrapped
   readonly?: true
}

type BlueprintProps = {
   type: 'blueprint'
   data: BlueprintUnwrapped
   readonly?: boolean
}

type GalleryStateContextType = [boolean, (isGalleryOpen: boolean) => void]

export const GalleryStateContext = createContext<GalleryStateContextType | null>(null)

export const Overview = ({
   type,
   data,
   readonly = true,
   onAcceptItemUpdateClick,
}: ComponentProps<(BlueprintProps | ItemProps) & OverviewProps>) => {
   const [isAddInterpretationModalOpen, setIsAddInterpretationModalOpen] = useState(false)
   const [isGalleryOpen, setIsGalleryOpen] = useState(false)

   data.interpretations.sort((interpretation) => (interpretation.pending ? 1 : -1))

   return (
      <>
         <GalleryStateContext.Provider value={[isGalleryOpen, setIsGalleryOpen]}>
            <div className="flex flex-col gap-10">
               <OverviewCard data={data} />
               <ScreenshotsFromSpace data={data} />
               <InterpretationList
                  interpretations={data.interpretations}
                  readonly={readonly}
                  blueprint={type === 'blueprint' ? data : undefined}
                  setIsAddInterpretationModalOpen={setIsAddInterpretationModalOpen}
                  onAcceptItemUpdateClick={onAcceptItemUpdateClick}
               />
            </div>
         </GalleryStateContext.Provider>
         {!readonly && type === 'blueprint' && (
            <AddInterpretationModal
               blueprint={data}
               open={isAddInterpretationModalOpen}
               onClose={() => setIsAddInterpretationModalOpen(false)}
            />
         )}
      </>
   )
}
