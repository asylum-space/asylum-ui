import { useCallback, useContext, useState } from 'react'

import { every, filter, includes, map } from 'lodash/fp'
import { useQuery } from 'react-query'
import { OptionProps, components } from 'react-select'

import { TagMetadata } from '@asylum-ui/connection-library'

import { fetchTags } from 'api'
import { ReactComponent as CheckmarkIcon } from 'assets/svg/checkmark.svg'
import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg'
import { Button } from 'components/button'
import { InputSelect } from 'components/input-select'
import { Tag } from 'components/tag'
import { HeadingXl, Paragraph } from 'components/text'
import { ColorContext } from 'context'
import { EditInterpretationModal } from 'modules/modal'
import { BlueprintUnwrapped, InterpretationUnwrapped } from 'types'
import { compareByTags } from 'utils/interpretationHelpers'

import { InterpretationCard } from './interpretation-card'

interface InterpretationListProps {
   interpretations: InterpretationUnwrapped[]
   blueprint?: BlueprintUnwrapped
   readonly: boolean
   setIsAddInterpretationModalOpen: (isOpen: boolean) => void
   onAcceptItemUpdateClick?: () => void
}

export const InterpretationList = ({
   interpretations,
   blueprint,
   readonly,
   setIsAddInterpretationModalOpen,
   onAcceptItemUpdateClick,
}: InterpretationListProps) => {
   const colorContextValue = useContext(ColorContext)

   const { data: tags = [] } = useQuery('tags', () => fetchTags())
   const interpretationsPending = interpretations
      .filter((interpretation) => interpretation.pending)
      .sort(compareByTags)
   const interpretationsSubmitted = interpretations
      .filter((interpretation) => !interpretation.pending)
      .sort(compareByTags)

   const [isEditInterpretationModalOpen, setIsEditInterpretationModalOpen] = useState(false)
   const [interpretationEdited, setInterpretationEdited] = useState<InterpretationUnwrapped | null>(
      null
   )

   const filterInterpretationsByTags = useCallback(
      (tags: TagMetadata[]): InterpretationUnwrapped[] => {
         if (!tags.length) {
            return interpretationsSubmitted
         }
         return filter(
            (interpretation) => every((tag) => includes(tag, interpretation.tags), map('id', tags)),
            interpretationsSubmitted
         )
      },
      [interpretations]
   )

   const [filterTags, setFilterTags] = useState<TagMetadata[]>([])
   useState<InterpretationUnwrapped | null>(null)

   return (
      <>
         <div className="flex flex-col gap-10">
            <div className="flex justify-between items-center my-3">
               <HeadingXl className="text-white">Interpretations</HeadingXl>
               {!readonly && (
                  <Button
                     variant="light"
                     onClick={() => {
                        setIsAddInterpretationModalOpen(true)
                     }}
                  >
                     <PlusIcon className="fill-text-base w-4 h-4 inline-block mr-2" /> add
                     interpretation
                  </Button>
               )}
            </div>

            <InputSelect<TagMetadata, true>
               surroundColor={colorContextValue}
               isMulti={true}
               placeholder="Filter by tags"
               name="tags"
               value={filterTags}
               onChange={(value) => setFilterTags(value as TagMetadata[])}
               options={tags}
               getOptionLabel={(option) => option.id}
               getOptionValue={(option) => option.id}
               components={{
                  Option,
               }}
            />

            <div className="flex flex-col gap-10" data-testid="interpretation-list">
               {interpretationsSubmitted.length === 0 && (
                  <div className="text-white">
                     <Paragraph className="flex gap-3 items-center">
                        No submitted interpretations found.
                     </Paragraph>
                  </div>
               )}
               {interpretationsSubmitted.length !== 0 &&
                  filterInterpretationsByTags(filterTags).length === 0 && (
                     <div className="text-white">
                        <Paragraph className="flex gap-3 items-center">
                           No interpretations found with set of tags:{' '}
                           <span className="flex gap-1">
                              {filterTags.map((tag) => (
                                 <Tag key={tag.id}>{tag.id}</Tag>
                              ))}
                           </span>
                        </Paragraph>
                     </div>
                  )}
               {filterInterpretationsByTags(filterTags)?.map((interpretation) => (
                  <InterpretationCard
                     key={interpretation.id}
                     interpretation={interpretation}
                     setIsEditInterpretationModalOpen={
                        !readonly ? setIsEditInterpretationModalOpen : undefined
                     }
                     setInterpretationEdited={setInterpretationEdited}
                  />
               ))}
            </div>
         </div>
         {!!interpretationsPending.length && (
            <div className="flex flex-col gap-10">
               <div className="flex justify-between items-center ">
                  <HeadingXl className="text-white">Pending Updates</HeadingXl>
                  <Button variant="light" onClick={onAcceptItemUpdateClick}>
                     <CheckmarkIcon className="fill-text-base w-4 h-4 inline-block mr-2" /> Approve
                     all updates
                  </Button>
               </div>

               <div className="flex flex-col gap-6">
                  {interpretationsPending?.map((interpretation) => (
                     <InterpretationCard
                        key={interpretation.id}
                        interpretation={interpretation}
                        setIsEditInterpretationModalOpen={
                           !readonly ? setIsEditInterpretationModalOpen : undefined
                        }
                        setInterpretationEdited={setInterpretationEdited}
                     />
                  ))}
               </div>
            </div>
         )}
         {!readonly && blueprint && (
            <EditInterpretationModal
               blueprintId={+blueprint.id}
               interpretation={interpretationEdited || undefined}
               open={isEditInterpretationModalOpen}
               onClose={() => {
                  setIsEditInterpretationModalOpen(false)
                  setInterpretationEdited(null)
               }}
            />
         )}
      </>
   )
}

const Option = (props: OptionProps<TagMetadata>) => {
   return (
      <components.Option {...props}>
         {props.label} <span className="text-gray-400">- {props.data.description}</span>
      </components.Option>
   )
}
