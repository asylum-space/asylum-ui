import { ReactElement, useContext } from 'react'

import { Gallery } from 'components/gallery'
import { ThreeViewer } from 'components/three-viewer'
import { GalleryStateContext } from 'modules/overview/overview'
import { ComponentProps, InterpretationUnwrapped } from 'types'
import { ipfsPath } from 'utils'
import { InterpretationViewType, defineInterpretationType } from 'utils/interpretationHelpers'

export const CarouselSupportedTypes: InterpretationViewType[] = ['image', '3d-model']

interface PreviewCarouselProps {
   interpretations: InterpretationUnwrapped[]
}

const thumbsGenerator = (node: ReactElement[]): ReactElement[] => {
   return node.map((nodeChild) => {
      const elem = nodeChild.props.children[1]
      switch (elem.props?.['data-type']) {
         case 'img':
            return (
               <img
                  {...elem.props}
                  key={elem.props.alt}
                  id={`${elem.props.alt}-thumb`}
                  alt={elem.props.alt}
               />
            )
         case '3d-model':
            return (
               <ThreeViewer
                  {...elem.props}
                  key={elem.props.alt}
                  id={`${elem.props.alt}-thumb`}
                  camera-controls={undefined}
                  interaction-prompt="none"
               />
            )
         default:
            throw new Error('unknown type')
      }
   })
}

export const RepresentationCarousel = ({
   interpretations,
   className,
}: ComponentProps<PreviewCarouselProps>) => {
   const isFullScreen = useContext(GalleryStateContext)?.[0]

   const constructView = (interpretation: InterpretationUnwrapped) => {
      const type = defineInterpretationType(interpretation)
      // Interpretation Representation Component cannot be used due to Carousel package limitations
      switch (type) {
         case 'image':
            return (
               <img
                  src={ipfsPath(interpretation.interpretationInfo.src)}
                  alt={interpretation.id}
                  data-type="img"
                  className="object-contain h-full"
               />
            )
         case '3d-model':
            return (
               <ThreeViewer
                  src={ipfsPath(interpretation.interpretationInfo.src)}
                  alt={interpretation.id}
                  data-type="3d-model"
                  autoplay={true}
                  auto-rotate={isFullScreen ? undefined : true}
                  rotation-per-second="15deg"
                  camera-controls={isFullScreen ? true : undefined}
                  className="object-contain h-full"
               />
            )
         default:
            return null
      }
   }

   return (
      <Gallery
         className={className}
         renderThumbs={(node) => thumbsGenerator(node as ReactElement[])}
      >
         {interpretations.map((interpretation) => (
            <div className="h-full pb-1" key={interpretation.id}>
               {interpretation.pending && (
                  <span className="bg-yellow-400 py-1 px-3 rounded-br text-white absolute left-0 top-0">
                     pending approval
                  </span>
               )}
               {constructView(interpretation)}
            </div>
         ))}
      </Gallery>
   )
}
