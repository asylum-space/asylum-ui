import { observer } from 'mobx-react-lite'
import { useQuery } from 'react-query'
import { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import { fetchSpaces } from 'api'
import { HeadingXl } from 'components/text/heading-xl'
import { useStore } from 'store'
import { BlueprintUnwrapped, ItemUnwrapped } from 'types'
import { isMatchingPattern } from 'utils'

import fireSword3d1 from './screenshots/fire-sword/dark-forest/1.png'
import fireSword3d2 from './screenshots/fire-sword/dark-forest/2.png'
import fireSword3d3 from './screenshots/fire-sword/dark-forest/3.png'
import fireSword1 from './screenshots/fire-sword/ninja-rian/1.png'
import fireSword2 from './screenshots/fire-sword/ninja-rian/2.png'
import fireSword3 from './screenshots/fire-sword/ninja-rian/3.png'
import sword3d1 from './screenshots/sword/dark-forest/1.png'
import sword3d2 from './screenshots/sword/dark-forest/2.png'
import sword3d3 from './screenshots/sword/dark-forest/3.png'
import sword1 from './screenshots/sword/ninja-rian/1.png'
import sword2 from './screenshots/sword/ninja-rian/2.png'
import sword3 from './screenshots/sword/ninja-rian/3.png'

interface ScreenshotsFromSpaceProps {
   data: BlueprintUnwrapped | ItemUnwrapped
}

export const ScreenshotsFromSpace = observer(({ data }: ScreenshotsFromSpaceProps) => {
   const store = useStore()

   const { data: spaces = [] } = useQuery(['spaces', store.account?.address], () =>
      store.account ? fetchSpaces(store.account) : []
   )

   const matchingSpaces = spaces.filter((space) =>
      space.patterns?.some((pattern) => isMatchingPattern(pattern, data))
   )

   const ninjaRianSwordScreenshots = [sword1, sword2, sword3]
   const darkForestSwordScreenshots = [sword3d1, sword3d2, sword3d3]
   const ninjaRianFireSwordScreenshots = [fireSword1, fireSword2, fireSword3]
   const darkForestFireSwordScreenshots = [fireSword3d1, fireSword3d2, fireSword3d3]

   let screenshotsArr: string[] = []

   if (('name' in data ? data.name : data.metadata.name).toLowerCase().includes('sword')) {
      if (('name' in data ? data.name : data.metadata.name).toLowerCase().includes('fire')) {
         if (matchingSpaces.filter((space) => space.title === 'Dark Forest').length) {
            screenshotsArr = screenshotsArr.concat(darkForestFireSwordScreenshots)
         }

         if (matchingSpaces.filter((space) => space.title === 'Ninja Rian').length) {
            screenshotsArr = screenshotsArr.concat(ninjaRianFireSwordScreenshots)
         }
      } else {
         if (matchingSpaces.filter((space) => space.title === 'Dark Forest').length) {
            screenshotsArr = screenshotsArr.concat(darkForestSwordScreenshots)
         }

         if (matchingSpaces.filter((space) => space.title === 'Ninja Rian').length) {
            screenshotsArr = screenshotsArr.concat(ninjaRianSwordScreenshots)
         }
      }
   }

   return (
      <div className="flex flex-col gap-5">
         {Boolean(screenshotsArr.length) && (
            <>
               <HeadingXl>Screenshots</HeadingXl>
               <Swiper
                  className="w-full"
                  slidesPerView={3}
                  navigation={true}
                  modules={[Navigation]}
               >
                  {screenshotsArr.map((imageSrc, index) => (
                     <SwiperSlide key={index}>
                        <img
                           src={imageSrc}
                           className="object-cover h-36 rounded-lg w-full"
                           alt=""
                        />
                     </SwiperSlide>
                  ))}
               </Swiper>
            </>
         )}
      </div>
   )
})
