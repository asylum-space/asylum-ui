import { useEffect } from 'react'

import { observer } from 'mobx-react-lite'
import { Route, Routes, useNavigate } from 'react-router-dom'

import { Layout } from 'layout/layout'
import { SpaceRunner } from 'modules/space-runner'
import { Blueprints } from 'pages/blueprints'
import { BlueprintOverview } from 'pages/blueprints/blueprint-overview'
import { Items } from 'pages/items'
import { ItemOverview } from 'pages/items/item-overview'
import { NotConnectedNetwork } from 'pages/not-connected-network'
import { NotConnectedWallet } from 'pages/not-connected-wallet'
import { SpaceOverview } from 'pages/space-overview'
import { BlueprintFromPattern } from 'pages/space-overview/blueprint-from-pattern'
import { SpaceSelection } from 'pages/space-selection'
import { useStore } from 'store'

const AppRoutes = () => (
   <Routes>
      <Route path="/" element={<SpaceSelection />} />
      <Route path="overview" element={<SpaceOverview />} />
      <Route path="overview/pattern/:patternName" element={<BlueprintFromPattern />} />
      <Route path="blueprints" element={<Blueprints />} />
      <Route path="blueprints/:id" element={<BlueprintOverview />} />
      <Route path="items" element={<Items />} />
      <Route path="items/:id" element={<ItemOverview />} />
   </Routes>
)

export const App = observer(() => {
   const store = useStore()
   const navigate = useNavigate()

   useEffect(() => {
      navigate('/')
   }, [store.account])

   return (
      <>
         <SpaceRunner />
         <Layout>
            {!store.isConnected ? (
               <NotConnectedNetwork />
            ) : !store.account ? (
               <NotConnectedWallet />
            ) : (
               <AppRoutes />
            )}
         </Layout>
      </>
   )
})
