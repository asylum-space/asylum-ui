FROM node:18.9.0-alpine as builder

WORKDIR /asylum-ui
COPY ./ ./

ENV ASYLUM_NODE_URL=ws://node-asylum:9944
ENV IPFS_NODE_URL=http://ipfs:5001

RUN yarn
RUN yarn build

EXPOSE 3000
